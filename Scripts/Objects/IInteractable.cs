﻿using UnityEngine;
using System.Collections;

namespace Farmind.Objects
{
    public interface IInteractable
    {
        void Interact();
    }
}