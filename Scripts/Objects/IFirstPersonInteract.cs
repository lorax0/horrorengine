﻿using UnityEngine;

namespace Farmind.Objects
{
    public interface IFirstPersonInteract : IInteractable
    {
        InteractObject InteractObject { get; }
    }
}