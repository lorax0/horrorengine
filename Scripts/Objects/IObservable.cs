﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.Objects
{
    public interface IObservable
    {
        ObservableObject ObservableObject { get; }
        void Observe();
    }
}