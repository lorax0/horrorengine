﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Farmind.Objects;

namespace Farmind.Objects.Interactable
{
    public class DoorUseBehaviour : MonoBehaviour, IInteractFunction
    {
        public void Use()
        {
            Debug.Log(transform.name);
        }
    }
}