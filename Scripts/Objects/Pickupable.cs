﻿using UnityEngine;

namespace Farmind.Objects
{
    [RequireComponent(typeof(Rigidbody), typeof(MeshRenderer))]
    public class Pickupable : MonoBehaviour, IPickupable
    {
        public Rigidbody Rigidbody { get => GetComponent<Rigidbody>(); }
        public Transform Transform { get => GetComponent<Transform>(); }
        public MeshRenderer MeshRenderer { get => GetComponent<MeshRenderer>(); }
    }
}