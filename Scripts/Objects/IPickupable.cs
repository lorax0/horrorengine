﻿using UnityEngine;

namespace Farmind.Objects
{
    public interface IPickupable
    {
        Rigidbody Rigidbody { get; }
        Transform Transform { get; }
        MeshRenderer MeshRenderer { get; }
    }
}