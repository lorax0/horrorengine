﻿using System;
using UnityEngine;

namespace Farmind.Objects
{
    [CreateAssetMenu(fileName = "ObserveObject", menuName = "Farmind/Objects/ObserveObject", order = 1)]
    public class ObservableObject : ScriptableObject
    {
        public Texture2D ObserveImage => this.observeImage;

        public string ObserveText => this.observeText;

        [SerializeField] protected Texture2D observeImage;
        [SerializeField] protected string observeText;
    }
}
