﻿using System;
using UnityEngine;

namespace Farmind.Objects
{
    [CreateAssetMenu(fileName = "InteractObject", menuName = "Farmind/Objects/InteractObject", order = 0)]
    public class InteractObject : ScriptableObject
    {
        public string ActionName => this.actionName;
        public Texture2D UseImage => this.useImage;
        public string UseText => this.useText;

        [SerializeField] protected string actionName;
        [SerializeField] protected Texture2D useImage;
        [SerializeField] protected string useText;
    }
}
