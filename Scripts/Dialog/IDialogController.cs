﻿using System;

namespace Farmind.Dialog
{
    public interface IDialogController
    {
        void PlayDialog();
        void StopDialog(object sender, EventArgs args);
    }
}