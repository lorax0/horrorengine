﻿using System.Collections.Generic;

namespace Farmind.Dialog
{
    public interface IDialog
    {
        IEnumerable<IDialogField> DialogFields { get; }
    }
}