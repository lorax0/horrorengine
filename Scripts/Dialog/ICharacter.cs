﻿namespace Farmind.Dialog
{
    public interface ICharacter
    {
        string CharacterName { get; }
    }
}