﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace Farmind.Dialog
{
    public interface IDialogDisplayManager
    {
        event EventHandler<EventArgs> OnDialogEnd;
        void DestoryAnswers();
        void CreateMessage(string dialogText, ICharacter character);
        void CreateAnswers(IEnumerable<IAnswer> dialogAnswers);
        void OnAnswerClick(IAnswer dialogAnswer);
        IEnumerator DisplayDialogFields(IEnumerable<IDialogField> dialogFields);
    }
}