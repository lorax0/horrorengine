﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.Dialog
{
    [CreateAssetMenu(fileName = "Character", menuName = "Farmind/Dialog/Character")]
    public class Character : ScriptableObject, ICharacter
    {
        public string CharacterName => this.characterName;
        [SerializeField] protected string characterName;
    }
}