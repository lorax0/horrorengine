﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.Dialog
{

    [CreateAssetMenu(fileName = "Dialog", menuName = "Farmind/Dialog")]
    public class Dialog : ScriptableObject, IDialog
    {
        public IEnumerable<IDialogField> DialogFields => this.dialogFields;
        [SerializeField] protected List<DialogField> dialogFields = new List<DialogField>();
    }
}