﻿using System.Collections.Generic;

namespace Farmind.Dialog
{
    public interface IDialogField
    {
        string Text { get; }
        ICharacter Character { get; }
        IEnumerable<IAnswer> Answers { get; }
        bool HaveAnswers();
    }
}