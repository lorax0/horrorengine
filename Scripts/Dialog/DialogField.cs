﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.Dialog
{
    [CreateAssetMenu(fileName = "DialogField", menuName = "Farmind/Dialog/DialogField")]
    public class DialogField : ScriptableObject, IDialogField
    {
        public string Text => this.text;
        public ICharacter Character => this.character;
        public IEnumerable<IAnswer> Answers => this.answers;
        [SerializeField] protected string text;
        [SerializeField] protected Character character;
        [SerializeField] protected List<Answer> answers = new List<Answer>();

        public bool HaveAnswers()
        {
            if(this.answers.Count != 0)
            {
                return true;
            }
            return false;
        }

    }
}