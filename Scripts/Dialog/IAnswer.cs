﻿using System.Collections.Generic;

namespace Farmind.Dialog
{
    public interface IAnswer
    {
        string Text { get; }
        IEnumerable<IDialogField> NextDialogs { get; }
        IEnumerable<IAnswer> NextAnswers { get; }
        bool HaveNextDialog();
        bool HaveNextAnswer();
    }
}