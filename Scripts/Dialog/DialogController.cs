﻿using UnityEngine;
using Farmind.Objects;
using System;

namespace Farmind.Dialog
{
    public class DialogController : MonoBehaviour, IFirstPersonInteract, IDialogController
    {
        public InteractObject InteractObject => this.interactObject;

        [SerializeField] protected InteractObject interactObject;
        [SerializeField] protected Dialog dialog;
        
        protected GameManager gameManager;
        protected DialogDisplayManager dialogManager;

        private void Start()
        {
            this.gameManager = GameManager.Instance;
            this.dialogManager = DialogDisplayManager.Instance;
            this.dialogManager.OnDialogEnd += this.StopDialog;
        }

        public void Interact() => this.PlayDialog();

        public void PlayDialog()
        {
            this.gameManager.LockControl();
            this.gameManager.UnlockScreen();
            this.dialogManager.ActiveDialogUI();
            this.DisplayDialog();
        }

        private void DisplayDialog()
        {
            var dialogFields = dialog.DialogFields;
            StartCoroutine(this.dialogManager.DisplayDialogFields(dialogFields));
        }

        public void StopDialog(object sender, EventArgs args)
        {
            this.gameManager.UnlockControl();
            this.gameManager.LockScreen();
        }

    }
}