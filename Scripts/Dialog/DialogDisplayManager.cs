﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Farmind.Dialog
{
    public class DialogDisplayManager : MonoBehaviour, IDialogDisplayManager
    {
        public static DialogDisplayManager Instance => instance;
        protected static DialogDisplayManager instance;

        public event EventHandler<EventArgs> OnDialogEnd;
        
        [SerializeField] protected Text messageTextPrefab;
        [SerializeField] protected Button answerButtonPrefab;
        [SerializeField] protected Character playerCharacter;
        [SerializeField] protected float buttonOffset;
        [SerializeField] protected GameObject chatPanel;
        [SerializeField] protected GameObject dialogUI;
        protected List<Button> buttons = new List<Button>();

        private void Awake()
        {
            if (instance == null)
            {
                instance = (DialogDisplayManager)FindObjectOfType(typeof(DialogDisplayManager));
                if (instance == null)
                {
                    Debug.LogWarning("An instance of " + typeof(DialogDisplayManager) + " is needed in the scene, but there is none.");
                }
            }
            this.OnDialogEnd += this.UnactiveDialogUI;
        }

        private void UnactiveDialogUI(object sender, EventArgs e)
        {
            this.dialogUI.SetActive(false);
        }

        public void ActiveDialogUI()
        {
            this.dialogUI.SetActive(true);
            this.DestoryAnswers();
        }

        public void DestoryAnswers()
        {
            foreach (var button in buttons)
            {
                Destroy(button.gameObject);
            }
            buttons.Clear();
        }

        public void CreateMessage(string dialogText, ICharacter character)
        {
            var messageText = Instantiate(this.messageTextPrefab, this.chatPanel.transform);
            messageText.text = character.CharacterName + ": "+ " " + dialogText;
        }

        public void CreateAnswers(IEnumerable<IAnswer> dialogAnswers)
        {
            for(int i=0;i<dialogAnswers.Count();i++)
            {
                var answerButton = Instantiate(this.answerButtonPrefab, this.chatPanel.transform);
                var answer = dialogAnswers.ElementAt(i);
                var answerText = answer.Text;
                this.buttons.Add(answerButton);
                answerButton.transform.position -= new Vector3(0, this.buttonOffset * i, 0);
                answerButton.GetComponent<Text>().text = answerText;
                answerButton.onClick.AddListener(() => this.OnAnswerClick(answer));
            }
        }

        public void OnAnswerClick(IAnswer dialogAnswer)
        {
            this.CreateMessage(dialogAnswer.Text, this.playerCharacter);

            if(!dialogAnswer.HaveNextDialog() && !dialogAnswer.HaveNextAnswer())
            {
                this.OnDialogEnd?.Invoke(this, new EventArgs { });
                return;
            }

            if(dialogAnswer.HaveNextDialog())
            {
                this.DestoryAnswers();
                StartCoroutine(this.DisplayDialogFields(dialogAnswer.NextDialogs));
            }

            else if (dialogAnswer.HaveNextAnswer())
            {
                this.DestoryAnswers();
                this.CreateAnswers(dialogAnswer.NextAnswers);
            }

            else if(dialogAnswer.HaveNextDialog() && dialogAnswer.HaveNextAnswer())
            {
                Debug.LogWarning("Dialog answer cannot have dialog answer and dialog field");
            }
        }

        public IEnumerator DisplayDialogFields(IEnumerable<IDialogField> dialogFields)
        {
            foreach (var field in dialogFields)
            {
                this.CreateMessage(field.Text, field.Character);
                yield return new WaitForSeconds(1f);
            }

            var lastIndex = dialogFields.Count() - 1;
            var lastDialogField = dialogFields.ElementAt(lastIndex);
            if (lastDialogField.HaveAnswers())
            {
                this.CreateAnswers(lastDialogField.Answers);
            }
            else
            {
                this.OnDialogEnd?.Invoke(this, new EventArgs { });
            }
        }
    }
}