﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace Farmind.Dialog
{
    [CreateAssetMenu(fileName = "Answer", menuName = "Farmind/Dialog/Answer")]
    public class Answer : ScriptableObject, IAnswer
    {
        public string Text => this.text;
        public IEnumerable<IDialogField> NextDialogs => this.dialogFields;
        public IEnumerable<IAnswer> NextAnswers => this.answers;
        [SerializeField] protected string text;
        [SerializeField] protected List<DialogField> dialogFields = new List<DialogField>();
        [SerializeField] protected List<Answer> answers = new List<Answer>();

        public bool HaveNextDialog() => this.dialogFields.Count != 0;

        public bool HaveNextAnswer() => this.answers.Count != 0;
    }
}