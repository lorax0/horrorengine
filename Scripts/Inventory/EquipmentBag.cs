﻿using Farmind.Inventory.Combain;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.IO;

namespace Farmind.Inventory
{
    [Serializable]
    public class EquipmentBag : IEquipmentBag
    {
        public IEquipmentItem EquippedItem
        {
            get
            {
                if (this.equippedOn != null)
                {
                    return this.equippedOn.GetItem();
                }

                return null;
            }
        }
        public int Capacity => this.slotsList.Count;
        public bool IsOpen => this.equipmentCanvas.IsOpen();

        public virtual ISlotsList SlotsList { get => this.slotsList; set => this.slotsList = (SlostsList)value; }
        public ICombainService CombainService => this.combainService;
        
        [SerializeField]
        protected SlostsList slotsList;
        [SerializeField]
        protected CombainService combainService;
        [SerializeField]
        protected GameObject playerHand;
        [SerializeField]
        protected GameObject Canvas;
        
        protected IEquipmentCanvasController equipmentCanvas;
        protected IEquipmentSlot equippedOn;
        protected int iconHandIndex = 0;

        public void Equip(IEquipmentSlot slot)
        {
            IEquipmentItem item = slot.GetItem();

            if (item != null)
            {
                var isNotAlreadyEquiped = !this.IsAlreadyEquiped(item);
                if (isNotAlreadyEquiped) this.Unequip();

                GameObject objectInHand = item.GameObject;
                if (objectInHand != null)
                {
                    objectInHand.transform.SetParent(playerHand.transform);
                    var positionOfObjectInHand = playerHand.transform.position;
                    objectInHand.transform.position = positionOfObjectInHand;
                    objectInHand.GetComponent<Renderer>().enabled = true;
                }

                this.equippedOn = slot;
                if (slot.DisplayService != null)
                {
                    slot.DisplayService.DisplayIcon(this.iconHandIndex);
                }

            }
            else
            {
                Debug.LogError("There is no object that should be in player hand!");
            }
        }

        public void Unequip()
        {
            if (this.equippedOn != null && !this.equippedOn.IsEmpty())
            {
                GameObject objectInHand = this.EquippedItem.GameObject;
                objectInHand.transform.SetParent(null);
                objectInHand.GetComponent<Renderer>().enabled = false;
                this.equippedOn.DisplayService.RemoveIcon(this.iconHandIndex);
                this.equippedOn = null;
            }
        }

        public void Toggle()
        {
            this.equipmentCanvas.Toggle(this.IsOpen);
        }

        public bool Add(IEquipmentItem item)
        {
            var emptySlot = this.SlotsList.GetEmpty();
            if (emptySlot == null)
            {
                return false;
            }
            emptySlot.AddItem(item);
            return true;
        }

        public bool IsAlreadyEquiped(IEquipmentItem item)
        {
            return this.EquippedItem == item;
        }

        public void Drop(IEquipmentSlot slot)
        {
            IEquipmentItem item = slot.GetItem();

            if (this.IsAlreadyEquiped(item))
            {
                this.Unequip();
            }

            slot.DropItem(playerHand.transform.position);
        }

        public void Combain(IEquipmentSlot slot)
        {
            this.combainService.Combain(slot);
        }

        public void Uncombain(IEquipmentSlot slot)
        {
            this.combainService.Uncombine(slot);
        }

        public void Initialize()
        {
            this.equipmentCanvas = Canvas.GetComponent<IEquipmentCanvasController>();
            this.equipmentCanvas.Hide();
            var slots = this.equipmentCanvas.Slots;
            this.slotsList.Slots = slots;
            this.CombainService.CombainList.Reset();
        }
    }
}
