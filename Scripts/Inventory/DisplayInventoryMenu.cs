using UnityEngine;
using UnityEngine.Events;

namespace Farmind.Inventory
{
    public class DisplayInventoryMenu : MonoBehaviour, IDisplayInventoryMenu
    {
        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        public void Show(Vector2 postion)
        {
            this.SetMenuToMousePosition(postion);
            this.gameObject.SetActive(true);
        }

        public void SetMenuToMousePosition(Vector2 postion)
        {
            RectTransform rectTransform = this.gameObject.GetComponent<RectTransform>();

            Vector2 pos;
            Canvas parentCanvas = this.transform.parent.GetComponent<Canvas>();

            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                parentCanvas.transform as RectTransform, postion,
                parentCanvas.worldCamera,
                out pos);

            pos.x += rectTransform.rect.width / 2;
            pos.y -= rectTransform.rect.height / 2;

            this.transform.position = parentCanvas.transform.TransformPoint(pos);
        }
    }
}