using Farmind.Inventory.Combain;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Farmind.Inventory
{

    [CreateAssetMenu(fileName = "EquipItem", menuName = "Farmind/Inventory/EquipItem")]
    public class Equipment : ScriptableObject, IEquipment
    {
        public string Name => this.name;
        public string Description => this.description;
        public Sprite Sprite => this.sprite;
        public IEnumerable<IEquipmentRecipe> Recipes => (IEnumerable<IEquipmentRecipe>)this.recipes;

        public GameObject Prefab => this.prefab;

        [SerializeField]
        protected List<EquipmentRecipe> recipes;
        [SerializeField]
        protected new string name;
        [SerializeField]
        protected Sprite sprite;
        [SerializeField]
        protected string description;
        [SerializeField]
        protected GameObject prefab;

        public ICombainResult Combain(IEnumerable<IEquipment> items)
        {
            var tempList = items.ToList();

            foreach (var recipe in this.recipes)
            {
                if (!recipe.IsValid())
                {
                    throw new Exception("Recipe " + recipe.Name + " is not valid");
                }

                if (!recipe.IsFirst(this))
                {
                    throw new Exception("Recipe combain material is not first");
                }

                if (!recipe.InRecipeWithCorrectOrder(items))
                {
                    continue;
                }


                if (recipe.Count == tempList.Count)
                {
                    recipe.Result.IsSuccess = true;
                    recipe.Result.ContinueCombain = false;
                    return recipe.Result;
                }

                recipe.Result.ContinueCombain = true;
                recipe.Result.IsSuccess = false;
                return recipe.Result;
            }

            var result = new CombainResult();
            result.ContinueCombain = false;
            result.IsSuccess = false;

            return result;
        }
    }
}