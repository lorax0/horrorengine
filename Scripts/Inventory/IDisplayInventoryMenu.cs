using UnityEngine;
using UnityEngine.Events;

namespace Farmind.Inventory
{
    public interface IDisplayInventoryMenu
    {
        void Show(Vector2 postion);
        void Hide();
        void SetMenuToMousePosition(Vector2 position);
    }
}