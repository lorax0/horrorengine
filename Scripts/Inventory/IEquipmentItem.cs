using UnityEngine;

namespace Farmind.Inventory
{
    /// <summary>
    /// If you want to add object to equipment bag it has to implement IEquipmentItem interface
    /// Interface for showing equipment object. It should used only to 
    /// maintain vsibility of objects and showing and hiding them 
    /// It connects unity game object with scriptable object equipment item
    /// </summary>
    public interface IEquipmentItem
    {
        string Name { get; }
        IEquipment Equipment { get; }
        GameObject GameObject { get; }
    }
}