using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Farmind.Inventory
{
    [Serializable]
    public class SlostsList : ISlotsList
    {
        public int Count => this.slots.Count;

        public virtual IEnumerable<IEquipmentSlot> Slots
        {
            get => this.slots;
            set
            {
                List<EquipmentSlot> list = new List<EquipmentSlot>();
                foreach (var slot in value)
                {
                    list.Add((EquipmentSlot)slot);
                }

                this.slots = list;
            }
        }

        protected List<EquipmentSlot> slots;

        public void Add(IEquipmentSlot slot)
        {
            this.slots.Add((EquipmentSlot)slot);
        }

        public IEquipmentSlot GetEmpty()
        {
            return this.Slots.Where(s => s.IsEmpty()).FirstOrDefault();
        }

        public IEquipmentSlot GetSlotWithItemName(string nameOfItem)
        {
            return this.Slots.Where(s => !s.IsEmpty() && s.GetItem().Name.Equals(nameOfItem)).FirstOrDefault();
        }
    }
}