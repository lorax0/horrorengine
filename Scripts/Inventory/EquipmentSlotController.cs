﻿using Farmind.EventsArgs;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Farmind.Inventory
{
    public class EquipmentSlotController : MonoBehaviour, IEquipmentSlotController, IPointerClickHandler
    {
        public IEquipmentSlot Slot { get => this.slot; }

        [SerializeField]
        protected EquipmentSlot slot;

        public event EventHandler<OnMouseButtonClickedEventArgs> OnRightClickMouseClicked;

        private void Start()
        {
            var display = this.slot.DisplayService;
            display.DeactivateNotEnabledIconGameObjects();
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                OnRightClickMouseClicked?.Invoke(this, new OnMouseButtonClickedEventArgs { Position = eventData.position });
            }
        }
    }
}
