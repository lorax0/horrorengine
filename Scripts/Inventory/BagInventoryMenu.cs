using UnityEngine;
using UnityEngine.UI;
using Farmind.EventsArgs;
using Farmind.Inventory.Combain;
using System.Collections.Generic;

namespace Farmind.Inventory
{
    [RequireComponent(typeof(DisplayInventoryMenu))]
    public class BagInventoryMenu : MonoBehaviour, IBagInventoryMenu
    {
        [SerializeField]
        protected Button equip;
        [SerializeField]
        protected Button unequip;
        [SerializeField]
        protected Button drop;
        [SerializeField]
        protected Button combain;
        [SerializeField]
        protected Button uncombain;

        protected IDisplayInventoryMenu displayInventoryMenu;
        protected IEquipmentBagController bagController;

        protected EquipmentSlot currentSlotClicked;
        [SerializeField]
        protected EquipmentSlotController[] slots;
        [SerializeField]
        protected EquipmentCanvasController canvas;

        public void Equip()
        {
            this.bagController.Bag.Equip(this.currentSlotClicked);
            this.canvas.UnhighlightAllSlots();
        }

        public void Drop()
        {
            this.bagController.Bag.Drop(this.currentSlotClicked);
            this.bagController.Bag.Uncombain(this.currentSlotClicked);
            this.canvas.UnhighlightAllSlots();
        }

        public void Combain()
        {
            this.bagController.Bag.Combain(this.currentSlotClicked);
            this.canvas.UnhighlightAllSlots();
        }

        public void Uncombain()
        {
            this.bagController.Bag.Uncombain(this.currentSlotClicked);
            this.canvas.UnhighlightAllSlots();
        }

        public void Unequip()
        {
            this.bagController.Bag.Unequip();
            this.canvas.UnhighlightAllSlots();
        }

        private void Start()
        {
            this.bagController = EquipmentBagController.Instance;
            this.displayInventoryMenu = this.gameObject.GetComponent<IDisplayInventoryMenu>();
            this.canvas = FindObjectOfType<EquipmentCanvasController>();
            this.slots = FindObjectsOfType<EquipmentSlotController>();

            this.Subscribe();
        }

        private void OnDestroy()
        {
            this.Unsubscribe();
        }

        private void Unsubscribe()
        {
            this.UnsubscribeFromSlotsClickEvents();
            this.UnsubscribeFromLeftClickOnCanvas();
        }

        private void UnsubscribeFromLeftClickOnCanvas()
        {
            this.canvas.OnLeftButtonMouseClicked -= this.OnLeftMouseClickedOnCanvas;
        }

        private void Subscribe()
        {
            this.SubscribeToSlotsClickEvents();
            this.SubsribeToLeftClickOnCanvasInventory();
        }

        private void UnsubscribeFromSlotsClickEvents()
        {
            foreach (var slot in this.slots)
            {
                slot.OnRightClickMouseClicked -= this.OnRightMouseClickedOnSlot;
            }
        }

        private void SubsribeToLeftClickOnCanvasInventory()
        {
            this.canvas.OnLeftButtonMouseClicked += this.OnLeftMouseClickedOnCanvas;
        }

        private void SubscribeToSlotsClickEvents()
        {

            foreach (var slot in this.slots)
            {
                slot.OnRightClickMouseClicked += this.OnRightMouseClickedOnSlot;
            }
        }

        private void OnRightMouseClickedOnSlot(object sender, OnMouseButtonClickedEventArgs args)
        {
            var slotController = (EquipmentSlotController)sender;
            this.currentSlotClicked = (EquipmentSlot)slotController.Slot;
            IDisplaySlotService displaySlot = this.currentSlotClicked.DisplayService;
            IEquipmentSlot slot = this.currentSlotClicked;

            if (!slot.IsEmpty())
            {
                IEquipmentItem item = slot.GetItem();
                ICombainService combainService = this.bagController.Bag.CombainService;
                bool combainActive = combainService.CombainList.IsCombainActive(this.currentSlotClicked);
                bool isEquipInteractable = this.bagController.Bag.IsAlreadyEquiped(item) ? false : true;
                this.equip.gameObject.SetActive(isEquipInteractable);
                this.unequip.gameObject.SetActive(!isEquipInteractable);
                this.combain.gameObject.SetActive(!combainActive);
                this.uncombain.gameObject.SetActive(combainActive);
                this.displayInventoryMenu.Show(args.Position);
                this.canvas.UnhighlightAllSlots();
                displaySlot.Highlight();
            }
        }

        private void OnLeftMouseClickedOnCanvas(object sender, OnMouseButtonClickedEventArgs args)
        {
            this.displayInventoryMenu.Hide();
            this.canvas.UnhighlightAllSlots();
        }
    }
}