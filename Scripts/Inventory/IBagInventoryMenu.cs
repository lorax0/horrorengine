namespace Farmind.Inventory
{
    public interface IBagInventoryMenu
    {
        void Equip();
        void Drop();
        void Combain();
        void Uncombain();
        void Unequip();
    }
}