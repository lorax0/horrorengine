using System;
using UnityEngine;

namespace Farmind.Inventory
{
    [Serializable]
    public class EquipmentItem : IEquipmentItem
    {
        public IEquipment Equipment => this.equipment; 

        public virtual GameObject GameObject => this.gameObject;
        
        public virtual string Name => this.GameObject.name;

        [SerializeField]
        protected string name;
        [SerializeField]
        protected Equipment equipment;
        [SerializeField]
        protected GameObject gameObject;
    }
}