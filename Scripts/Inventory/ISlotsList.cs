using System.Collections.Generic;

namespace Farmind.Inventory
{
    public interface ISlotsList
    {
        int Count { get; }
        void Add(IEquipmentSlot slot);
        IEquipmentSlot GetEmpty();
        IEnumerable<IEquipmentSlot> Slots { get; set; }
        IEquipmentSlot GetSlotWithItemName(string nameOfItem);
    }
}