using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Farmind.Inventory
{
    public interface IEquipmentBagController
    {
        IEquipmentBag Bag { get; set; }
    }
}