﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmind.Inventory
{
    public interface IEquipmentCanvasController
    {
        List<IEquipmentSlot> Slots { get; }
        bool IsOpen();
        void Toggle(bool isOpen);
        void Hide();
        void UnhighlightAllSlots();
    }
}
