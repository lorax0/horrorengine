﻿using Farmind.EventsArgs;
using System;


namespace Farmind.Inventory
{
    public interface IEquipmentSlotController
    {
        IEquipmentSlot Slot { get; }
        event EventHandler<OnMouseButtonClickedEventArgs> OnRightClickMouseClicked;
    }
}
