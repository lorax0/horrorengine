﻿using Farmind.Inventory.Combain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Farmind.Inventory
{
    public interface IEquipmentBag
    {
        /// <summary>
        /// Capacity is amount of slots that is currently inside 
        /// the bag, and how many items we can storage there.
        /// </summary>
        /// <value></value>
        int Capacity { get; }
        ISlotsList SlotsList { get; set; }
        ICombainService CombainService { get; }
        IEquipmentItem EquippedItem { get; }
        /// <summary>
        /// Method for checking is inventory bag currently opened
        /// </summary>
        /// <value>
        /// Returns current state of showing the bag inventory,
        /// false if close, true if open
        /// </value>
        bool IsOpen { get; }
        /// <summary>
        /// Method for showing and hidding inventory bag, it's called toggle, so if 
        /// inventory is showed we just hide and vice versa
        /// </summary>
        void Toggle();
        /// <summary>
        /// Method for adding the item into the bag
        /// </summary>
        /// <param name="item">Accepts display equipment item, with gameobject reference</param>
        /// <returns>True if added with success ,otherwise false</returns>
        bool Add(IEquipmentItem item);
        /// <summary>
        /// Method for equiping the item from inventory.
        /// </summary>
        /// <param name="slot">
        /// Accepts gameobject with slots component inside
        /// </param>
        /// <returns>True if is equipped,otherwise false</returns>
        void Equip(IEquipmentSlot slot);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="slot"> Accepts gameobject with slots components inside</param>
        /// <returns></returns>
        void Unequip();
        void Drop(IEquipmentSlot slot);
        void Combain(IEquipmentSlot slot);
        void Uncombain(IEquipmentSlot slot);
        bool IsAlreadyEquiped(IEquipmentItem item);
        void Initialize();
    }
}
