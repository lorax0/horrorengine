﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Farmind.Inventory
{
    public interface IEquipmentItemController
    {
        EquipmentItem Item { get; }
    }
}
