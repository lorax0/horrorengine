using UnityEngine;

namespace Farmind.Inventory
{
    public interface IEquipmentActionIcon
    {
        string Name { get; set; }
        Sprite Icon { get; set; }
    }
}