using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Farmind.Inventory
{
    [Serializable]
    public class DisplaySlotService : IDisplaySlotService
    {
        public IEnumerable<IEquipmentActionIcon> ActionIcons => this.actionIcons;
        public Image Image { get => this.Image; set => this.image = value; }
        public Transform Transform { get => this.transform; set => transform = value; }

        [SerializeField]
        protected List<EquipmentActionIcon> actionIcons = new List<EquipmentActionIcon>();
        [SerializeField]
        protected Image image;
        [SerializeField]
        protected Transform transform;
        [SerializeField]
        protected Transform actionTransform;
        [SerializeField]
        protected Color highlightColor;
        [SerializeField]
        protected Color defaultColor;

        
        public void DeactivateNotEnabledIconGameObjects()
        {
            for (var i = 0; i < this.actionTransform.childCount; i++)
            {
                var child = this.actionTransform.GetChild(i);
                if (!this.IsIconActive(i))
                {
                    child.gameObject.SetActive(false);
                }
            }
        }

        public void SetImageSprite(IEquipment item)
        {
            image.sprite = item.Sprite;
            image.enabled = true;
        }

        public void RemoveImageSprite()
        {
            this.image.sprite = null;
            image.enabled = false;
        }

        public Image GetIcon(int index)
        {
            Image image = null;

            var child = this.actionTransform.GetChild(index);
            if (child != null)
            {
                image = child.GetComponent<Image>();
            }

            return image;
        }

        public void DisplayIcon(string nameOfIcon)
        {
            for (var i = 0; i < this.actionIcons.Count; i++)
            {
                var actionIcon = this.actionIcons[i];
                if (actionIcon.Name == nameOfIcon)
                {
                    this.DisplayIcon(i);
                }
            }
        }

        public void RemoveIcons()
        {
            for (var i = 0; i < this.actionIcons.Count; i++)
            {
                var image = this.GetIcon(i);
                image.sprite = null;
                image.enabled = false;
            }
        }

        public void RemoveIcons(string nameOfIcon)
        {
            for (var i = 0; i < this.actionIcons.Count; i++)
            {
                var actionIcon = this.actionIcons[i];
                if (actionIcon.Name == nameOfIcon)
                {
                    this.RemoveIcon(i);
                }
            }
        }

        public void DisplayIcon(int index)
        {
            var actionIcon = this.actionIcons[index];
            var image = this.GetIcon(index);
            image.sprite = actionIcon.Icon;
            image.enabled = true;
            image.gameObject.SetActive(true);
        }

        public void RemoveIcon(int index)
        {
            var image = this.GetIcon(index);
            image.sprite = null;
            image.enabled = false;
            image.gameObject.SetActive(false);
        }

        public bool IsIconActive(int index)
        {
            return this.actionTransform.GetChild(index).gameObject.GetComponent<Image>().enabled;
        }

        public bool IsIconActive(string icon)
        {
            for (var i = 0; i < actionTransform.childCount; i++)
            {
                var child = transform.GetChild(i);
                if (child.name == icon)
                {
                    return child.GetComponent<Image>().enabled;
                }
            }

            return false;
        }

        public void Highlight()
        {
            var image = this.transform.GetComponent<Image>();
            image.color = this.highlightColor;
        }

        public void Unhighlight()
        {
            var image = this.transform.GetComponent<Image>();
            image.color = this.defaultColor;
        }
    }
}
