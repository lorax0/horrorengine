using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Farmind.Inventory
{
    public interface IDisplaySlotService
    {
        Transform Transform { get; set; }
        Image Image { get; set; }
        void DeactivateNotEnabledIconGameObjects();
        IEnumerable<IEquipmentActionIcon> ActionIcons { get; }
        void DisplayIcon(string nameOfIcon);
        void DisplayIcon(int index);
        void RemoveIcons();
        void RemoveIcons(string nameOfIcon);
        void RemoveIcon(int index);
        void SetImageSprite(IEquipment item);
        void RemoveImageSprite();
        Image GetIcon(int index);
        bool IsIconActive(int index);
        bool IsIconActive(string icon);
        void Highlight();
        void Unhighlight();
    }
}