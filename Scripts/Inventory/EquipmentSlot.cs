using System;
using UnityEngine;

namespace Farmind.Inventory
{

    [Serializable]
    public class EquipmentSlot : IEquipmentSlot
    {
        protected IEquipmentItem item;

        [SerializeField]
        protected DisplaySlotService display;
        
        protected GameObject inside;

        public IDisplaySlotService DisplayService { get => this.display; set => this.display = (DisplaySlotService)value; }

        public void AddItem(IEquipmentItem item)
        {
            this.item = item;
            this.inside = item.GameObject;
            if(inside != null)
            {
                this.inside.GetComponent<Renderer>().enabled = false;
                this.inside.GetComponent<Collider>().enabled = false;
            }
            this.display?.SetImageSprite(item.Equipment);
        }

        public void LoadItem(IEquipmentItem item) => this.AddItem(item);

        public virtual IEquipmentItem GetItem()
        {
            return this.item;
        }

        public void DropItem(Vector3 position)
        {
            this.inside.transform.position = position;
            this.DropItem();
        }

        public void DropItem()
        {
            this.inside.GetComponent<Renderer>().enabled = true;
            this.inside.GetComponent<Collider>().enabled = true;
            this.RemoveItem();
        }
        
        public void RemoveItem()
        {
            this.display?.RemoveImageSprite();
            this.item = null;
            this.inside = null;
        }
        
        public bool IsEmpty() => this.GetItem() == null;

        public void DestroyItem()
        {
            this.display.RemoveIcons();
            GameObject.Destroy(this.inside);
            this.RemoveItem();
        }
    }
}