﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Farmind.Inventory;
using UnityEngine;
using Farmind.Objects;

namespace Farmind.Inventory
{
    public class EquipmentItemController : MonoBehaviour, IEquipmentItemController, IFirstPersonInteract, IObservable
    {
        public EquipmentItem Item => this.item;

        public InteractObject InteractObject => this.interactObject;

        public ObservableObject ObservableObject => this.observableObject;

        [SerializeField] protected EquipmentItem item;
        [SerializeField] protected InteractObject interactObject;
        [SerializeField] protected ObservableObject observableObject;

        protected IEquipmentItemController equipmentItemDisplay;
        protected IEquipmentBagController bagManager;

        private void Start()
        {
            this.bagManager = EquipmentBagController.Instance;
            this.equipmentItemDisplay = this.gameObject.GetComponent<IEquipmentItemController>();
            if (this.interactObject == null)
            {
                Debug.LogWarning("Interact object is not set");
            }
            if (this.observableObject == null)
            {
                Debug.LogWarning("Observable object is not set");
            }
        }

        public void Interact() => this.TakeItem();

        public void TakeItem()
        {
            IEquipmentItem item = this.equipmentItemDisplay.Item;
            this.bagManager.Bag.Add(item);
        }

        public void Observe()
        {
            var equipment = item.Equipment;
            Debug.Log(equipment.Description);
        }
    }
}
