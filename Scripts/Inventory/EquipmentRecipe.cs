using Farmind.Inventory.Combain;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Farmind.Inventory
{

    [CreateAssetMenu(fileName = "Recipe", menuName = "Farmind/Inventory/Recipe")]
    public class EquipmentRecipe : ScriptableObject, IEquipmentRecipe
    {
        public IEnumerable<IEquipment> Materials => this.materials;
        public ICombainResult Result => this.result;
        public int Count => this.materials.Count;
        public string Name { get => this.name; set => this.name = value; }
        [SerializeField]
        [Min(2)]
        protected List<Equipment> materials;
        [SerializeField]
        protected CombainResult result;
        [SerializeField]
        protected new string name;

        public bool IsValid() => this.Count > 1;
        public bool InRecipe(IEquipment item) => this.materials.Contains((Equipment)item);
        public bool IsFirst(IEquipment item) => this.materials.FirstOrDefault() == (Equipment)item && item != null;
        public bool InRecipeWithCorrectOrder(IEnumerable<IEquipment> items)
        {
            var tempList = items.ToList();
            foreach (var item in tempList)
            {
                if (!this.InRecipe(item))
                {
                    return false;
                }

                var index = tempList.IndexOf(item);
                var materialIndex = this.materials.IndexOf(((Equipment)item));

                if (index != materialIndex)
                {
                    return false;
                }
            }

            return true;
        }
    }
}