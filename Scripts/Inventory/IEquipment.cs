using Farmind.Inventory.Combain;
using UnityEngine;

namespace Farmind.Inventory
{
    public interface IEquipment : ICombainable
    {
        string Name { get; }
        Sprite Sprite { get; }
        string Description { get; }
    }
}