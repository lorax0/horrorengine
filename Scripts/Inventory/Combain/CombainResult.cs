﻿using System;
using UnityEngine;

namespace Farmind.Inventory.Combain
{
    [Serializable]
    public class CombainResult : ICombainResult
    {
        public GameObject Prefab => this.prefab;
        public bool IsSuccess { get => this.isSuccess; set => this.isSuccess = value; }
        public bool ContinueCombain { get => this.continueCombain; set => this.continueCombain = value; }

        [SerializeField]
        protected GameObject prefab;
        protected bool isSuccess;
        protected bool continueCombain;
    }
}
