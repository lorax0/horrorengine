﻿using System;
using UnityEngine;

namespace Farmind.Inventory.Combain
{
    public interface ICombainService
    {
        ICombainList CombainList { get; }
        void Combain(IEquipmentSlot slot);
        void Uncombine(IEquipmentSlot slot);
        void CreateCombainResult(ICombainResult result);
    }
}
