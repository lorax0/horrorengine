﻿using System.Collections.Generic;

namespace Farmind.Inventory.Combain
{
    public interface ICombainList
    {
        int Count { get; }
        IEquipmentSlot FirstSlot { get; }
        IEquipment FirstItem { get; set; }
        IEnumerable<IEquipment> Items { get; }
        void Reset();
        void Add(IEquipmentSlot slot);
        bool IsCombainActive(IEquipmentSlot slot);
        void DestroyMaterials();
    }
}
