﻿using System;
using UnityEngine;

namespace Farmind.Inventory.Combain
{
    [Serializable]
    public class CombainService : ICombainService
    {
        public ICombainList CombainList => this.combainList;
        [SerializeField]
        protected CombainList combainList;
        public void Combain(IEquipmentSlot slot)
        {
            IEquipmentItem equipmentItem = slot.GetItem();
            if (equipmentItem != null)
            {
                this.combainList.FirstItem = equipmentItem.Equipment;
                this.combainList.Add(slot);
                var hasBeenRemoved = this.combainList.FirstItem == null;
                var haveNotEnoguhItems = this.combainList.Count <= 1;
                if (hasBeenRemoved || haveNotEnoguhItems)
                {
                    return;
                }

                var result = this.combainList.FirstItem.Combain(this.combainList.Items);

                if (result.ContinueCombain)
                {
                    return;
                }
                
                if (result.IsSuccess)
                {
                    this.CreateCombainResult(result);
                    this.combainList.DestroyMaterials();
                }

                this.combainList.Reset();
            }
        }
        
        public void Uncombine(IEquipmentSlot slot)
        {
            var combain = this.combainList.IsCombainActive(slot);
            if (combain)
            {
                this.combainList.Remove(slot);
            }
        }

        public void CreateCombainResult(ICombainResult result)
        {
            if (!result.IsSuccess)
            {
                return;
            }

            var slot = this.combainList.FirstSlot;
            if (slot != null)
            {
                var createdItem = GameObject.Instantiate(result.Prefab);
                var equipmentItem = createdItem.GetComponent<IEquipmentItemController>().Item;
                if (equipmentItem == null)
                {
                    throw new Exception("There is no display equipment component inside item!");
                }

                var item = slot.GetItem();

                var gameObject = item.GameObject;
                GameObject.Destroy(gameObject);
                slot.RemoveItem();
                slot.AddItem(equipmentItem);
            }
        }
     
    }
}
