﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Farmind.Inventory.Combain
{
    [Serializable]
    public class CombainList : ICombainList
    {
        public IEquipment FirstItem { get => this.firstItem; set { if (this.firstItem == null) { this.firstItem = (Equipment)value; } } }
        public IEquipmentSlot FirstSlot { get => this.slots.FirstOrDefault(); }
        public int Count => this.items.Count;
        public IEnumerable<IEquipment> Items => this.items;

        [SerializeField]
        protected List<EquipmentSlot> slots;
        [SerializeField]
        protected Equipment firstItem;
        [SerializeField]
        protected List<Equipment> items;
        
        protected int combainIconIndex = 1;

        public void Add(IEquipmentSlot slot)
        {
            if (slot != null)
            {
                slot.DisplayService.DisplayIcon(this.combainIconIndex);
                var item = slot.GetItem();
                if (item != null)
                {
                    this.items.Add((Equipment)item.Equipment);
                }

                this.slots.Add((EquipmentSlot)slot);
            }
            
        }

        public void Remove(IEquipmentSlot slot)
        {
            if (slot != null)
            {
                if (this.IsCombainActive(slot))
                {
                    slot.DisplayService.RemoveIcon(this.combainIconIndex);
                    var item = slot.GetItem();

                    if (item != null)
                    {
                        this.RemoveFirstItemIfFirstHaveGotClicked(slot,item);
                        this.items.Remove((Equipment)item.Equipment);
                    }

                    this.slots.Remove((EquipmentSlot)slot);
                }
            }
        }

        private void RemoveFirstItemIfFirstHaveGotClicked(IEquipmentSlot slot,IEquipmentItem  item)
        {
            var isFirstClicked = (Equipment)item.Equipment == this.firstItem && this.FirstSlot == slot;
            if (isFirstClicked)
            {
                this.firstItem = null;
                this.Reset();
            }
        }

        public void Reset()
        {
            if (this.slots != null)
            {
                foreach (var combainSlot in this.slots)
                {
                    combainSlot.DisplayService.RemoveIcon(this.combainIconIndex);
                }
            }

            this.slots = new List<EquipmentSlot>();
            this.items = new List<Equipment>();
            this.firstItem = null;
        }

        public bool IsCombainActive(IEquipmentSlot slot)
        {
            return slot.DisplayService.IsIconActive(this.combainIconIndex);
        }

        public void DestroyMaterials()
        {
            foreach (var slot in this.slots)
            {
                if (slot != this.FirstSlot)
                {
                    slot.DestroyItem();
                }
            }
        }
    }
}
