using System.Collections.Generic;

namespace Farmind.Inventory.Combain
{
    public interface ICombainable
    {
        /// <summary>
        /// List of recipes that is used to combain, item
        /// </summary>
        /// <value></value>
        IEnumerable<IEquipmentRecipe> Recipes { get; }
        ICombainResult Combain(IEnumerable<IEquipment> items);
    }
}