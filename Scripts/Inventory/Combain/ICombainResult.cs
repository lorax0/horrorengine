﻿using UnityEngine;

namespace Farmind.Inventory.Combain
{
    public interface ICombainResult
    {
        bool IsSuccess { get; set; }
        bool ContinueCombain { get; set; }
        GameObject Prefab { get; }

    }
}
