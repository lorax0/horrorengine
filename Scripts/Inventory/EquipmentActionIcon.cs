using UnityEngine;

namespace Farmind.Inventory
{
    [CreateAssetMenu(fileName = "ActionIcon", menuName = "Farmind/Inventory/ActionIcon")]
    public class EquipmentActionIcon : ScriptableObject, IEquipmentActionIcon
    {
        public string Name { get => this.name; set => this.name = value; }
        public Sprite Icon { get => this.icon; set => this.icon = value; }

        [SerializeField]
        protected new string name;
        [SerializeField]
        protected Sprite icon;
    }
}