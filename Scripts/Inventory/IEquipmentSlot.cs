using System;
using Farmind.EventsArgs;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Farmind.Inventory
{
    public interface IEquipmentSlot
    {
        IDisplaySlotService DisplayService { get; set; }
        /// <summary>
        /// Add item to slot
        /// </summary>
        /// <param name="item">Item to add</param>
        /// <returns> Result of adding, return true with success and false with failure</returns>
        void AddItem(IEquipmentItem item);
        /// <summary>
        /// Remove item from slot
        /// </summary>
        /// <returns>Removed item from slot</returns>
        void DropItem();
        void DropItem(Vector3 position);
        /// <summary>
        ///  Use it if you want to recieve the item from slot,
        /// and not remove it.
        /// </summary>
        /// <returns> Item from slot </returns>
        IEquipmentItem GetItem();
        void LoadItem(IEquipmentItem item);
        void RemoveItem();
        void DestroyItem();
        bool IsEmpty();
    }
}