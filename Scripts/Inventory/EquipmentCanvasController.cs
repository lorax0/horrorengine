using Farmind.EventsArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Farmind.Inventory
{
    public class EquipmentCanvasController : MonoBehaviour, IPointerClickHandler, IEquipmentCanvasController
    {

        public List<IEquipmentSlot> Slots
        {
            get
            {
                List<IEquipmentSlotController> slotsController = this.GetComponentsInChildren<IEquipmentSlotController>().ToList();
                List<IEquipmentSlot> slots = new List<IEquipmentSlot>();

                foreach (var controller in slotsController)
                {
                    slots.Add(controller.Slot);
                }

                return slots;
            }
        }

        public event EventHandler<OnMouseButtonClickedEventArgs> OnLeftButtonMouseClicked;

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        public bool IsOpen() =>this.gameObject.activeSelf;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left)
            {
                OnLeftButtonMouseClicked?.Invoke(this,
                    new OnMouseButtonClickedEventArgs { Position = eventData.position });
            }
        }

        public void Toggle(bool isOpen)
        {
            this.OnLeftButtonMouseClicked?.Invoke(this, new OnMouseButtonClickedEventArgs { });
            this.gameObject.SetActive(!isOpen);
        }

        public void UnhighlightAllSlots()
        {
            foreach(var slot in Slots)
            {
                slot.DisplayService.Unhighlight();
            }
        }
    }
}