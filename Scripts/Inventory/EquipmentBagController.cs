using Farmind.Inventory.Combain;
using NSubstitute.Routing.Handlers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Farmind.Inventory
{

    public class EquipmentBagController : MonoBehaviour, IEquipmentBagController
    {
        public IEquipmentBag Bag { get => this.bag; set => this.bag = (EquipmentBag) value; }

        [SerializeField]
        protected EquipmentBag bag;

        public static IEquipmentBagController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (EquipmentBagController)FindObjectOfType(typeof(EquipmentBagController));
                    if (instance == null)
                    {
                        Debug.LogWarning("An instance of " + typeof(EquipmentBagController) + " is needed in the scene, but there is none.");
                    }
                }
                return instance;
            }
        }

        protected static EquipmentBagController instance;

        void Start()
        {
            this.Bag.Initialize();
        }

    }
}