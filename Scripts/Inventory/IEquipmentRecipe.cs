using Farmind.Inventory.Combain;
using System.Collections.Generic;

namespace Farmind.Inventory
{
    public interface IEquipmentRecipe
    {
        string Name { get; set; }
        IEnumerable<IEquipment> Materials { get; }
        ICombainResult Result { get; }
        int Count { get; }
        bool IsFirst(IEquipment item);
        bool IsValid();
        bool InRecipe(IEquipment item);
        bool InRecipeWithCorrectOrder(IEnumerable<IEquipment> items);
    }
}