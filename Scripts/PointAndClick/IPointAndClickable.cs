﻿using UnityEngine;
using Farmind.Objects;

namespace Farmind.PointAndClick
{
    public interface IPointAndClickable : IInteractable
    {
        string Name { get; }
    }
}