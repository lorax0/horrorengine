﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Farmind.PointAndClick
{
    [Serializable]
    public class ItemMenuController : IItemMenuController
    {
        public GameObject ItemMenu => this.itemMenu;
        [SerializeField] protected GameObject itemMenu;

        protected IEnumerable<Button> menuButtons;

        public void SetButtons()
        {

            this.menuButtons = this.itemMenu.GetComponentsInChildren<Button>();
            foreach (var button in this.menuButtons)
            {
                button.gameObject.SetActive(false);
            }
        }

        public void SetMenu(IEnumerable<IPointAndClickable> interactableObjects)
        {
            if (interactableObjects.Count() > this.menuButtons.Count()) Debug.LogError("Not enough buttons");
            for (int i = 0; i < interactableObjects.Count(); i++)
            {
                var button = this.menuButtons.ElementAt(i);
                var interactableObject = interactableObjects.ElementAt(i);
                button.gameObject.SetActive(true);
                button.onClick.AddListener(() => interactableObject.Interact());
                button.GetComponentInChildren<Text>().text = interactableObject.Name;
            }
        }

        public void DisableMenu()
        {
            foreach(var button in this.menuButtons)
            {
                button.onClick.RemoveAllListeners();
                button.gameObject.SetActive(false);
            }
        }

        public void SetMenuPosition(Vector3 position)
        {
            this.itemMenu.transform.position = position;
        }
    }
}