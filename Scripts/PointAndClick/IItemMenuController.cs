﻿using System.Collections.Generic;
using UnityEngine;

namespace Farmind.PointAndClick
{
    public interface IItemMenuController
    {
        GameObject ItemMenu { get; }

        void DisableMenu();

        void SetMenu(IEnumerable<IPointAndClickable> interactableObjects);

        void SetMenuPosition(Vector3 position);
    }
}