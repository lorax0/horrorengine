﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Farmind.Objects;
using Farmind.Player.Input;

namespace Farmind.PointAndClick
{
    public class CameraController : MonoBehaviour, ICameraController , IFirstPersonInteract
    {
        public Camera ThirdCamera => thirdCamera;
        public InteractObject InteractObject => this.interactObject;
        public ICameraSelectionService CameraSelectionController => this.cameraSelectionController;

        [SerializeField] protected Camera thirdCamera;
        [SerializeField] protected InteractObject interactObject;

        protected ICameraSelectionService cameraSelectionController;
        protected IItemMenuController itemMenuController;
        protected GameManager gameManager;
        protected InputActionManager inputActionManager;

        private void Start()
        {
            this.cameraSelectionController = PointAndClick.CameraSelectionService.Instance;
            this.gameManager = GameManager.Instance;
            this.inputActionManager = InputActionManager.Instance;
            this.itemMenuController = this.cameraSelectionController.ItemMenuController;
            this.cameraSelectionController.ClickInputAction.action.started += this.Check;
        }

        public void Interact()
        {
            if (this.cameraSelectionController.IsCameraSwitched())
            {
                this.cameraSelectionController.BackCameraToMain();
                this.cameraSelectionController.ItemMenuController.DisableMenu();
                this.gameManager.UnlockControl();
                this.gameManager.LockScreen();
                return;
            }
            this.cameraSelectionController.SwitchCamera(this.thirdCamera);
            this.gameManager.LockControl();
            this.gameManager.UnlockScreen();
        }
        
        public void Check(InputAction.CallbackContext callbackContext)
        {
            if (!this.cameraSelectionController.IsCameraSwitched()) return;

            Ray ray = this.cameraSelectionController.GetCurrentCamera().ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                this.itemMenuController.DisableMenu();
                var interactableObjects = hit.collider?.GetComponents<IPointAndClickable>();
                if (interactableObjects != null)
                {
                    var currentCamera = this.cameraSelectionController.GetCurrentCamera();
                    this.itemMenuController.SetMenuPosition(currentCamera.WorldToScreenPoint(hit.transform.position));
                    this.itemMenuController.SetMenu(interactableObjects);
                }
            }
        }
    }
}