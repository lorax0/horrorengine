﻿using UnityEngine;

namespace Farmind.PointAndClick
{
    public interface ICameraController
    {
        Camera ThirdCamera { get; }
        ICameraSelectionService CameraSelectionController { get; }
    }
}