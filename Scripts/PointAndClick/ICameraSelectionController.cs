﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Farmind.PointAndClick
{
    public interface ICameraSelectionService
    {
        Camera FirstPersonCamera { set; }
        InputActionReference ClickInputAction { get; }
        IItemMenuController ItemMenuController { get; }

        bool IsCameraSwitched();

        void SwitchCamera(Camera camera);

        void BackCameraToMain();

        Camera GetCurrentCamera();
    }
}