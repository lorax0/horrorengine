﻿using UnityEngine;
using System;
using Farmind.Player;
using UnityEngine.InputSystem;

namespace Farmind.PointAndClick
{
    [Serializable]
    public class CameraSelectionService : ICameraSelectionService
    {
        public static CameraSelectionService Instance => instance;
        protected static CameraSelectionService instance;

        public Camera FirstPersonCamera { set => this.firstPersonCamera = value; }
        public InputActionReference ClickInputAction => this.clickInputAction;
        public IItemMenuController ItemMenuController => this.itemMenuController;

        [SerializeField] protected InputActionReference clickInputAction;
        [SerializeField] protected ItemMenuController itemMenuController;

        protected Camera firstPersonCamera = null;
        protected Camera thirdPersonCamera = null;

        public void SetStartValue()
        {
            if(this.itemMenuController.ItemMenu == null)
            {
                Debug.LogWarning("Pointnclick item menu missing");
                return;
            }
            this.SetSingleton();
            this.itemMenuController.SetButtons();
        }

        public void SetSingleton()
        {
            if (instance == null)
            {
                instance = (CameraSelectionService) MonoBehaviour.FindObjectOfType<PlayerController>().CameraSelectionService;
                if (instance == null)
                {
                    Debug.LogWarning("An instance of " + typeof(PlayerController) + " is needed in the scene, but there is none.");
                }
            }
        }

        public bool IsCameraSwitched() => this.thirdPersonCamera != null;

        public void SwitchCamera(Camera camera)
        {
            this.thirdPersonCamera = camera;
            this.firstPersonCamera.enabled = false;
            this.thirdPersonCamera.enabled = true;
        }

        public void BackCameraToMain()
        {
            this.firstPersonCamera.enabled = true;
            this.thirdPersonCamera.enabled = false;
            this.thirdPersonCamera = null;
        }

        public Camera GetCurrentCamera()
        {
            return this.thirdPersonCamera != null ? this.thirdPersonCamera : this.firstPersonCamera;
        }
    }
}