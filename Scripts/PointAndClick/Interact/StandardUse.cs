﻿using UnityEngine;
using Farmind.Objects;

namespace Farmind.PointAndClick
{
    public class StandardUse : MonoBehaviour, IPointAndClickable
    {
        public string Name => this.name;

        [SerializeField] protected new string name;

        public void Interact()
        {
            var interactObject = this.GetComponent<IInteractFunction>();
            interactObject.Use();
        }
    }
}