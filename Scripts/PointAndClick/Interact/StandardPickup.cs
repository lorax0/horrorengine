﻿using Farmind.Inventory;
using UnityEngine;

namespace Farmind.PointAndClick
{
    public class StandardPickup : MonoBehaviour, IPointAndClickable
    {
        public string Name => this.name;

        [SerializeField] protected new string name;

        protected IEquipmentBag equipmentBag;
        protected IEquipmentItem equipmentItem;
       
        private void Start()
        {
            this.equipmentBag = EquipmentBagController.Instance.Bag;
            this.equipmentItem = this.GetComponent<IEquipmentItemController>().Item;
        }
        
        public void Interact()
        {
            if (this.equipmentItem != null) return;

            bool succesfullyAdded = this.equipmentBag.Add(this.equipmentItem);
            if(succesfullyAdded) this.gameObject.SetActive(false);
        }
    }
}