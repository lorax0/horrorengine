﻿using UnityEngine;
using System.Collections;
using System;

namespace Farmind.PointAndClick
{
    public class StandardMove : MonoBehaviour, IPointAndClickable
    {
        public string Name => this.name;

        [SerializeField] protected new string name;
        [SerializeField] protected Vector3 destinationPosition;
        [SerializeField] protected float speed = 5f;
        protected Vector3 startPosition;
        protected const float speedMultiplier = 0.1f;

        private void Start()
        {
            this.startPosition = this.transform.position;
        }

        public void Interact()
        {
            StartCoroutine(this.Move());
        }

        private IEnumerator Move()
        {
            var destinationPosition = this.SetDestinationPosition();
            var getPosition = false;
            while (!getPosition)
            {
                var dir = destinationPosition - this.transform.position;
                this.transform.Translate(dir * Time.deltaTime * this.speed * speedMultiplier, Space.World);
                if (Vector3.Distance(this.transform.position, destinationPosition) < 0.01f)
                {
                    getPosition = true;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        private Vector3 SetDestinationPosition()
        {
            if(Vector3.Distance(this.transform.position, this.destinationPosition) < 0.1f)
            {
                return this.startPosition;
            }
            return this.destinationPosition;
        }
    }
}