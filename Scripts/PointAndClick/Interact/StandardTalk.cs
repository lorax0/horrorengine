﻿using UnityEngine;

namespace Farmind.PointAndClick
{
    public class StandardTalk : MonoBehaviour, IPointAndClickable
    {
        public string Name => this.name;

        [SerializeField] protected new string name;
        [SerializeField] protected string Dialogue = "";
        
        public void Interact()
        {
            Debug.Log(Dialogue);
        }
    }
}