﻿using System;
using UnityEngine;

namespace Farmind.PointAndClick
{
    public class StandardObserve : MonoBehaviour, IPointAndClickable
    {
        public string Name => this.name;

        [SerializeField] protected new string name;
        [SerializeField] protected string TextToDisplay = "";

        public void Interact()
        {
            Debug.Log(TextToDisplay);
        }
    }
}