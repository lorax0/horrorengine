﻿using Farmind.Objects;
using UnityEngine;

namespace Farmind.Player.Mechanic
{
    public interface IMechanicSystem
    {
        IPickupable HoldingObj { get; set; }
        Transform PlayerTransform { get; set; }
        float Distance { get; set; }

        Transform CameraTransform { get; }
        Quaternion ObjRotationWhenPickup { get; }
        float MaxObjectPickupDistance { get; }
        float Smoothness { get; }
        float AngleThrowOffset { get; }
        LayerMask BackgroundLayerMask { get; }
        bool IsHoldingItem { get; }


        void SetupMechanicCalculation();
        void Pickup();
        void Hold();
        void Throw();
        void Drop();
    }
}