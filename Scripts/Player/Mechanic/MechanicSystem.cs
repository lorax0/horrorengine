﻿using UnityEngine;
using Farmind.Objects;

namespace Farmind.Player.Mechanic
{
    [System.Serializable]
    public class MechanicSystem : IMechanicSystem
    {
        private const int Multiplier = 100;

        public IPickupable HoldingObj { get; set; }
        public Transform PlayerTransform { get; set; }
        public float Distance { get; set; }

        public Transform CameraTransform => cameraTransform;
        public Quaternion ObjRotationWhenPickup => _objRotationWhenPickup;
        public float MaxObjectPickupDistance => maxObjectPickupDistance;
        public float Smoothness => smoothness;
        public float AngleThrowOffset => angleThrowOffset;
        public LayerMask BackgroundLayerMask => LayerMask.GetMask("Background");

        public bool IsHoldingItem => HoldingObj is IPickupable;

        [SerializeField] private Transform cameraTransform = null;
        [SerializeField] private SpringJoint springJoint = null;
        [SerializeField] private float maxObjectPickupDistance = 2.5f;
        [SerializeField] [Range(1, 100)] private float smoothness = 100f;
        [SerializeField] [Range(0, 100)] private int powerThrow = 20;
        [SerializeField] [Range(0, 30)] private float angleThrowOffset = 15f;
        [SerializeField] [Range(10, 100)] private float powerRotate = 25f;
        [SerializeField] private MechanicCalculation mechanicCalculation;

        private Quaternion _objRotationWhenPickup;

        public void SetupMechanicCalculation() => this.mechanicCalculation.MechanicSystem = this;

        public void Pickup()
        {
            bool IsObjectInMaxPickupDistanceFromCamera = Physics.Raycast(this.CameraTransform.position, this.CameraTransform.TransformDirection(Vector3.forward), out RaycastHit hit, maxObjectPickupDistance);
            if (IsObjectInMaxPickupDistanceFromCamera)
            {
                if (hit.collider.TryGetComponent(out IPickupable pickupObj))
                    this.ConnectObjectToPlayer(pickupObj);
            }
        }

        private void ConnectObjectToPlayer(IPickupable pickupObject)
        {
            springJoint.connectedBody = pickupObject.Rigidbody;
            pickupObject.Rigidbody.isKinematic = true;
            _objRotationWhenPickup = pickupObject.Transform.rotation;
            HoldingObj = pickupObject;
        }

        public void Hold()
        {
            this.Distance = this.mechanicCalculation.CalculateDistance();
            HoldingObj.Transform.position = this.mechanicCalculation.CalculatePosition();
            HoldingObj.Transform.localRotation = this.mechanicCalculation.CalculateRotation();
        }

        public void Throw()
        {
            HoldingObj.Rigidbody.isKinematic = false;
            Vector3 force = this.mechanicCalculation.CalculateThrowVector();
            HoldingObj.Rigidbody.AddForce(force * powerThrow * Multiplier);
            this.Drop(this.PlayerTransform.forward);
        }

        public void Drop() => this.Drop(null);

        private void Drop(Vector3? turn = null)
        {
            if (turn is null) turn = this.PlayerTransform.up;

            this.DisconnectObjectFromPlayer((Vector3)turn);
        }

        private void DisconnectObjectFromPlayer(Vector3 turn)
        {
            HoldingObj.Rigidbody.isKinematic = false;
            HoldingObj.Rigidbody.AddTorque(turn * powerRotate);
            springJoint.connectedBody = null;
            HoldingObj = null;
        }
    }
}
