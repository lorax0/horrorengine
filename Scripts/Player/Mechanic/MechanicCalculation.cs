﻿using Farmind.Objects;
using UnityEngine;

namespace Farmind.Player.Mechanic
{
    [System.Serializable]
    public class MechanicCalculation
    {
        public IMechanicSystem MechanicSystem { get; set; }
        public IPickupable Pickupable => this.MechanicSystem.HoldingObj;


        public float CalculateDistance()
        {
            float distance = this.MechanicSystem.MaxObjectPickupDistance;
            float halfDiagonal = this.Pickupable.MeshRenderer.bounds.extents.magnitude * Mathf.Sqrt(2);

            bool IsObjectCollideWithBackground = Physics.Raycast(this.MechanicSystem.CameraTransform.position, 
                this.MechanicSystem.CameraTransform.TransformDirection(Vector3.forward), 
                out RaycastHit hit, this.MechanicSystem.MaxObjectPickupDistance + halfDiagonal, this.MechanicSystem.BackgroundLayerMask);

            if (IsObjectCollideWithBackground)
                distance = Vector3.Distance(hit.point, this.MechanicSystem.CameraTransform.position) - halfDiagonal;
            return distance;
        }

        public Vector3 CalculatePosition()
        {
            return Vector3.Lerp(this.Pickupable.Transform.position, 
                this.MechanicSystem.CameraTransform.position + (this.MechanicSystem.CameraTransform.forward * this.MechanicSystem.Distance), 
                Time.deltaTime * this.MechanicSystem.Smoothness);
        }

        public Quaternion CalculateRotation()
        {
            Quaternion startObjectRotation = this.MechanicSystem.ObjRotationWhenPickup;
            Quaternion lookAt = Quaternion.LookRotation(this.Pickupable.Transform.position - this.MechanicSystem.PlayerTransform.position);
            Quaternion playerRotation = Quaternion.Euler(this.MechanicSystem.CameraTransform.rotation.x, this.MechanicSystem.PlayerTransform.rotation.y, 0f);
            Quaternion objectRotation = lookAt * startObjectRotation * playerRotation;

            return Quaternion.Lerp(this.Pickupable.Transform.rotation, objectRotation, Time.deltaTime * this.MechanicSystem.Smoothness);
        }

        public Vector3 CalculateThrowVector()
        {
            return this.MechanicSystem.PlayerTransform.forward + new Vector3(0f, this.MechanicSystem.CameraTransform.rotation.x + this.MechanicSystem.AngleThrowOffset, 0f).normalized;
        }
    }
}
