﻿using System;
using UnityEngine;
using Farmind.EventsArgs;
using System.Collections.Generic;

namespace Farmind.Player.Movement
{
    [Serializable]
    public class HeadBobbing : IHeadBobbing
    {
        public float Sensitivity => this.sensitivity;

        public event EventHandler<OnHeadBobbingValueChanged> OnValueChanged;
        [SerializeField] protected float sensitivity = 20f;
        protected const float multiplier = 0.001f;
        protected float walkingTime;

        public void HeadBobOffSet(float speed)
        {
            var offSet = Vector3.zero;
            if (speed == 0)
            {
                this.walkingTime = 0;
                OnValueChanged?.Invoke(this, new OnHeadBobbingValueChanged { OffSet = offSet });
                return;
            }
            this.walkingTime += speed * Time.deltaTime;
            offSet = this.CalculateHeadBobOffSet(speed);
            OnValueChanged?.Invoke(this, new OnHeadBobbingValueChanged { OffSet = offSet });
        }

        public Vector3 CalculateHeadBobOffSet(float speed)
        {
            var offSet = Vector3.zero;
            var horizontalOffSet = 0f;
            var verticalOffSet = 0f;

            if(this.walkingTime <= 0)
            {
                return offSet;
            }
            horizontalOffSet = Mathf.Cos(this.walkingTime) * speed * multiplier * this.sensitivity;
            verticalOffSet = Mathf.Sin(this.walkingTime * 2) * speed * multiplier * this.sensitivity;
            offSet = new Vector3(horizontalOffSet, verticalOffSet);
            return offSet;
        }
    }
}
