﻿using UnityEngine;
using System;
using Farmind.EventsArgs;

namespace Farmind.Player.Movement
{
    public interface IHeadBobbing
    {
        float Sensitivity { get; }
        event EventHandler<OnHeadBobbingValueChanged> OnValueChanged;
        void HeadBobOffSet(float speed);
        Vector3 CalculateHeadBobOffSet(float speed);
    }
}