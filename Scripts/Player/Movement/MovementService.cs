﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using Farmind.Player.Input;
using System;

namespace Farmind.Player.Movement
{
    [Serializable]
    public class MovementService : IMovementService
    {
        public float RunningSpeed { get => this.runningSpeed; set => this.runningSpeed = value; }
        public float MovementSpeed { get => this.movementSpeed; set => this.movementSpeed = value; }
        public float CrouchingSpeed { get => this.crouchingSpeed; set => this.crouchingSpeed = value; }
        public float CrouchOffSet { get => this.crouchOffSet; set => this.crouchOffSet = value; }
        public float HorizonatalSpeedMultiplier { get => this.horizonatalSpeedMultiplier; set => this.horizonatalSpeedMultiplier = value; }
        public InputAction CrouchAction { get => this.crouchAction; set => this.crouchAction = value; }
        public InputAction SprintAction { set => this.sprintAction = value;}
        public InputActionManager InputActionManager { set => this.inputActionManager = value; }
        public HeadBobbing HeadBobbing => this.headBobbing;

        [SerializeField] protected float runningSpeed = 15f;
        [SerializeField] protected float movementSpeed = 10f;
        [SerializeField] protected float crouchingSpeed = 5f;
        [SerializeField] protected float crouchOffSet = 0.5f;
        [SerializeField] protected float horizonatalSpeedMultiplier = 0.5f;
        [SerializeField] protected HeadBobbing headBobbing;

        protected float currentSpeed;
        protected InputActionManager inputActionManager;
        protected InputAction crouchAction;
        protected InputAction sprintAction;
        
        public Vector3 MoveDirection(Vector2 moveInput)
        {
            Vector3 dir = Vector3.zero;
            var speed = this.GetMovementSpeed();

            if(speed != this.runningSpeed) dir = new Vector3(moveInput.x * this.horizonatalSpeedMultiplier, 0, moveInput.y) * speed;
            else dir = new Vector3(moveInput.x * this.movementSpeed, 0, moveInput.y * speed);

            if (dir == Vector3.zero) this.headBobbing?.HeadBobOffSet(0);
            else this.headBobbing?.HeadBobOffSet(speed * 0.2f);

            return dir;
        }

        public float GetMovementSpeed()
        {
            if (this.IsRunning())
            {
                return this.runningSpeed;
            }
            if (this.IsCrouching())
            {
                return this.crouchingSpeed;
            }
            return this.movementSpeed;
        }

        public virtual bool IsCrouching()
        {
            var crouchingButtonPressed = this.inputActionManager.IsPressedButton(this.crouchAction);
            if (crouchingButtonPressed)
            {
                return true;
            }
            return false;
        }

        public virtual bool IsRunning()
        {
            var runningButtonPressed = this.inputActionManager.IsPressedButton(this.sprintAction);
            if (runningButtonPressed)
            {
                return true;
            }
            return false;
        }
    }
}