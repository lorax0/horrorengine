﻿using UnityEngine;

namespace Farmind.Player.Movement
{
    public interface IMovementService
    {
        float RunningSpeed { get; set; }

        float MovementSpeed { get; set; }

        float CrouchingSpeed { get; set; }

        Vector3 MoveDirection(Vector2 moveInput);

        float GetMovementSpeed();

        bool IsCrouching();

        bool IsRunning();
    }
}