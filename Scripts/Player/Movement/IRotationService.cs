﻿using UnityEngine;

namespace Farmind.Player.Movement
{
    public interface IRotationService
    {
        float Sensitivity { get; set; }

        float MaxYAngle { get; set; }

        float MinYAngle { get; set; }

        Vector3 Rotation(Vector2 mouseInput);
    }
}