using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System;

namespace Farmind.Player.Movement
{
    [Serializable]
    public class RotationService : IRotationService
    {
        public float Sensitivity { get => this.sensitivity; set => this.sensitivity = value; }
        public float MaxYAngle { get => this.maxYAngle; set => this.maxYAngle = value; }
        public float MinYAngle { get => this.minYAngle; set => this.minYAngle = value; }

        [SerializeField] protected float sensitivity = 5f;
        [SerializeField] protected float maxYAngle = 60f;
        [SerializeField] protected float minYAngle = -60f;

        protected float rotationY = 0f;
        protected float rotationX = 0f;

        public Vector3 Rotation(Vector2 mouseInput)
        {
            this.rotationY -= mouseInput.y * this.sensitivity;
            this.rotationY = Mathf.Clamp(this.rotationY, this.minYAngle, this.maxYAngle);
            this.rotationX += mouseInput.x * this.sensitivity;

            var rotation = new Vector3(this.rotationY, this.rotationX, 0);
            return rotation;
        }
    }
}