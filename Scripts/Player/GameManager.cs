﻿using UnityEngine;
using UnityEngine.InputSystem;
using System;
using System.Collections.Generic;

[Serializable]
public class GameManager
{
    public static GameManager Instance => instance;
    protected static GameManager instance;

    public IEnumerable<InputActionReference> ActionsForBlock => this.actionsForBlock;

    [SerializeField] protected List<InputActionReference> actionsForBlock;

    public void SetSingleton()
    {
        if (instance == null)
        {
            instance = MonoBehaviour.FindObjectOfType<GameManagerController>().GameManager;
            if (instance == null)
            {
                Debug.LogWarning("An instance of " + typeof(GameManagerController) + " is needed in the scene, but there is none.");
            }
        }
    }

    public void LockScreen()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public void UnlockScreen()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    public void ToogleScreenLock()
    {
        if(Cursor.lockState == CursorLockMode.Locked)
        {
            this.UnlockScreen();
            return;
        }
        this.LockScreen();
    }

    public void LockControl()
    {
        foreach(var action in this.actionsForBlock)
        {
            action.action.Disable();
        }

    }

    public void UnlockControl()
    {
        foreach (var action in this.actionsForBlock)
        {
            action.action.Enable();
        }
    }

    public void ToogleControl()
    {
        if (this.HaveControl())
        {
            this.LockControl();
            return;
        }
        this.UnlockControl();
    }

    public bool HaveControl()
    {
        foreach (var action in this.actionsForBlock)
        {
            if (!action.action.enabled)
            {
                return false;
            }
        }
        return true;
    }

    public void EnableAction(InputAction showInventoryAction) => showInventoryAction.Enable();
}
