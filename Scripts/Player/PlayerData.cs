﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Farmind.Inventory;
using System.IO;
using System;

namespace Farmind.Player
{
    [Serializable]
    public class PlayerData : IPlayerData
    {
        public Vector3 PlayerPosition { get => this.playerPosition; set => this.playerPosition = value; }
        public Quaternion PlayerRotation { get => this.playerRotation; set => this.playerRotation = value; }
        public IEquipmentBag EquipmentBag { get => this.equipmentBag; set => this.equipmentBag = (EquipmentBag)value; }

        public event EventHandler<EventArgs> OnPlayerLoad;
        public event EventHandler<EventArgs> OnPlayerSave;

        protected EquipmentBag equipmentBag;

        protected Vector3 playerPosition;
        protected Quaternion playerRotation;

        public void Save(string path)
        {
            this.OnPlayerSave?.Invoke(this, new EventArgs { });

            using (StreamWriter streamWriter = File.CreateText(path))
            {
                var playerPosition = this.playerPosition;
                var playerRotation = this.playerRotation;
                string positionJson = JsonUtility.ToJson(playerPosition);
                string rotationJson = JsonUtility.ToJson(playerRotation);
                streamWriter.WriteLine(positionJson);
                streamWriter.WriteLine(rotationJson);
                
                var slots = this.equipmentBag.SlotsList;
                foreach (var slot in slots.Slots)
                {
                    var item = slot.GetItem();
                    string json = JsonUtility.ToJson(item);
                    streamWriter.WriteLine(json);
                }
            }
        }

        public void Load(string path)
        {
            if (!File.Exists(path))
            {
                Debug.LogWarning("Save does not exist");
                return;
            }

            using (StreamReader streamReader = File.OpenText(path))
            {
                var positionJson = streamReader.ReadLine();
                var rotationJson = streamReader.ReadLine();
                var playerPosition = JsonUtility.FromJson<Vector3>(positionJson);
                var playerRotation = JsonUtility.FromJson<Quaternion>(rotationJson);
                this.playerPosition = playerPosition;
                this.playerRotation = playerRotation;
                
                ISlotsList slots = this.equipmentBag.SlotsList;
                foreach (var slot in slots.Slots)
                {
                    var json = streamReader.ReadLine();
                    var item = JsonUtility.FromJson<EquipmentItem>(json);
                    slot.RemoveItem();
                    if (item == null)
                    {
                        continue;
                    }
                    slot.LoadItem(item);
                }
                this.OnPlayerLoad?.Invoke(this, new EventArgs { });
                this.equipmentBag.Unequip();
            }
        }
        
    }
}
