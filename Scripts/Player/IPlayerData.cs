﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Farmind.Inventory;

namespace Farmind.Player
{
    public interface IPlayerData
    {
        Vector3 PlayerPosition { get; set; }
        Quaternion PlayerRotation { get; set; }
        IEquipmentBag EquipmentBag { get; set; }
        
        void Save(string directory);
        void Load(string directory);
    }
}