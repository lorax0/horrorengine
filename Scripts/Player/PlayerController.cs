﻿using UnityEngine;
using System;
using UnityEngine.InputSystem;
using Farmind.Player.Input;
using Farmind.Inventory;
using Farmind.PointAndClick;
using Farmind.EventsArgs;
using Farmind.Player.Movement;

namespace Farmind.Player
{
    public class PlayerController : MonoBehaviour, IPlayerController
    {
        public IPlayerData PlayerData { get => this.playerData; set => this.playerData = (PlayerData) value; }
        public IEquipmentBagController Equipment { get => this.equipment; set => this.equipment = (EquipmentBagController)value; }
        public MovementService MovementService { get => this.movementService; set => this.movementService = (MovementService)value; }
        public ICameraSelectionService CameraSelectionService => this.cameraSelectionService;
        [SerializeField] protected Camera playerCamera;
        [SerializeField] protected RotationService rotationService;
        [SerializeField] protected MovementService movementService;
        [SerializeField] protected CameraSelectionService cameraSelectionService;
        protected PlayerData playerData = new PlayerData();
        protected EquipmentBagController equipment;
        protected InputActionManager inputActionManager;
        protected InputActionMap playerAction;
        protected InputActionMap uiAction;
        protected InputAction cameraSwitchAction;
        protected InputAction showInventoryAction;
        protected InputAction moveAction;
        protected InputAction lookAction;
        protected GameManager gameManager;
        protected CharacterController characterController;

        private void Start()
        {
            this.inputActionManager = InputActionManager.Instance;
            this.gameManager = GameManager.Instance;
            this.equipment = (EquipmentBagController)EquipmentBagController.Instance;
            this.characterController = this.gameObject.GetComponent<CharacterController>();
            this.playerAction = this.inputActionManager.PlayerActionMap;
            this.uiAction = this.inputActionManager.UIActionMap;
            this.cameraSelectionService.FirstPersonCamera = this.playerCamera;
            this.movementService.InputActionManager = this.inputActionManager;
            this.movementService.CrouchAction = this.playerAction.FindAction("Crouch");
            this.movementService.SprintAction = this.playerAction.FindAction("Sprint");
            this.cameraSwitchAction = this.uiAction.FindAction("CameraSwitch");
            this.showInventoryAction = this.uiAction.FindAction("Show Inventory");
            this.moveAction = this.playerAction.FindAction("Move");
            this.lookAction = this.playerAction.FindAction("Look");
            this.cameraSelectionService.SetStartValue();
            this.SetPlayerData();
            this.SubscribeEvents();
        }

        private void Update()
        {
            if (this.CanMove())
            {
                this.Move();
                this.Rotate();
            }
            this.ToggleInventory();
        }

        public bool CanMove()
        {
            var canMove = this.gameManager.HaveControl();
            return canMove;
        }

        private void SubscribeEvents()
        {
            this.SubscribeMovementEvents();
        }

        private void SubscribeMovementEvents()
        {
            var crouchAction = this.movementService.CrouchAction;
            crouchAction.started += this.OnCrouchBegin;
            crouchAction.canceled += this.OnCrouchEnd;
            this.movementService.HeadBobbing.OnValueChanged += this.OnHeadBobbingValueChanged;
        }

        private void SetPlayerData()
        {
            this.playerData.OnPlayerLoad += this.LoadPlayer;
            this.playerData.OnPlayerSave += this.SavePlayer;
            this.playerData.EquipmentBag = this.equipment.Bag;
        }
        

        private void ToggleInventory()
        {
            var buttonPressed = this.inputActionManager.WasPressedButtonThisFrame(this.showInventoryAction);
            if (buttonPressed)
            {
                this.equipment.Bag.Toggle();
                this.gameManager.ToogleScreenLock();
                this.gameManager.ToogleControl();
                this.gameManager.EnableAction(this.showInventoryAction);
            }
        }

        public void Move()
        {
            var moveActionForce = this.moveAction.ReadValue<Vector2>();
            Vector3 dir = this.transform.rotation * this.movementService.MoveDirection(moveActionForce) * Time.deltaTime;
            this.characterController.SimpleMove(dir * this.movementService.MovementSpeed);
        }

        public void Rotate()
        {
            var rotationForce = this.lookAction.ReadValue<Vector2>();
            var rotation = this.rotationService.Rotation(rotationForce * Time.deltaTime);
            this.transform.rotation = Quaternion.Euler(new Vector3(0, rotation.y, 0));
            this.playerCamera.transform.localRotation = Quaternion.Euler(new Vector3(rotation.x, 0, 0));
        }

        private void OnHeadBobbingValueChanged(object sender, OnHeadBobbingValueChanged args)
        {
            this.playerCamera.transform.localPosition = args.OffSet;
        }

        private void OnCrouchBegin(InputAction.CallbackContext callbackContext)
        {
            this.playerCamera.transform.position -= new Vector3(0, this.movementService.CrouchOffSet, 0);
            this.characterController.height -= this.movementService.CrouchOffSet;
        }

        private void OnCrouchEnd(InputAction.CallbackContext callbackContext)
        {
            this.playerCamera.transform.position += new Vector3(0, this.movementService.CrouchOffSet, 0);
            this.characterController.height += this.movementService.CrouchOffSet;
        }

        private void LoadPlayer(object sender, EventArgs args)
        {
            this.transform.position = this.playerData.PlayerPosition;
            this.transform.rotation = this.playerData.PlayerRotation;
            this.equipment.Bag = this.playerData.EquipmentBag;
        }

        private void SavePlayer(object sender, EventArgs args)
        {
            this.playerData.PlayerPosition = this.transform.position;
            this.playerData.PlayerRotation = this.transform.rotation;
            this.playerData.EquipmentBag = this.equipment.Bag;
        }
    }
}