﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Farmind.Inventory;
using Farmind.Player.Movement;

namespace Farmind.Player
{
    public interface IPlayerController
    {
        IPlayerData PlayerData { get; set; }
        IEquipmentBagController Equipment { get; set; }
        MovementService MovementService { get; set; }
        bool CanMove();
        void Move();
        void Rotate();
    }
}