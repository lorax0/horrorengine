﻿using System;
using UnityEngine;
using Farmind.Player.Input;

public class GameManagerController : MonoBehaviour
{
    public GameManager GameManager => this.gameManager; 
    public InputActionManager InputActionManager => this.inputActionManager; 
    [SerializeField] protected GameManager gameManager;
    [SerializeField] protected InputActionManager inputActionManager;

    private void Awake()
    {
        this.gameManager.SetSingleton();
        this.inputActionManager.SetSingleton();
        this.inputActionManager.SetActionMaps();
        this.gameManager.LockScreen();
    }
}
