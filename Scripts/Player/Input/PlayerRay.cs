﻿using System.Collections.Generic;
using Farmind.Translation;
using Farmind.PointAndClick;
using Farmind.Objects;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

namespace Farmind.Player.Input
{
    public class PlayerRay : MonoBehaviour
    {
        [SerializeField] protected TranslationService TranslationService;

        [SerializeField] protected bool showInteractionIcon = true;
        [SerializeField] protected float rayDistance = 5.0f;
        [SerializeField] protected Texture2D defaultPointerImage;

        protected Texture2D pointerImage;

        protected InputActionMap playerMapActions;
        protected InputActionMap uiMapActions;
        protected GameManager gameManager;
        protected InputActionManager inputActionManager;
        protected InputAction observeAction;
        
        void Start()
        {
            this.inputActionManager = InputActionManager.Instance;
            this.gameManager = GameManager.Instance;
            this.pointerImage = this.defaultPointerImage;
            this.playerMapActions = this.inputActionManager.PlayerActionMap;
            this.uiMapActions = this.inputActionManager.UIActionMap;
            this.observeAction = this.playerMapActions.FindAction("Observe");
        }

        void Update()
        {
            Vector3 frontDir = this.transform.TransformDirection(Vector3.forward);
            Debug.DrawRay(this.transform.position, frontDir * rayDistance, Color.green);
            RaycastHit hit;
            if (Physics.Raycast(this.transform.position, frontDir, out hit, rayDistance))
            {
                this.UseObject(hit.transform.gameObject);
                this.ObserveObject(hit.transform.gameObject);
                return;
            }
            this.SetDefaultValue();
        }

        private void ObserveObject(GameObject gameObject)
        {
            var observableItem = gameObject.GetComponent<IObservable>();
            var observableObject = observableItem?.ObservableObject;
            if (observableItem == null || observableObject == null)
            {
                return;
            }
            if (this.showInteractionIcon)
            {
                this.pointerImage = observableObject.ObserveImage;
            }
            else
            {
                this.ShowTextPerLanguage(observableObject.ObserveText + "Observed");
            }
            
            if (this.inputActionManager.WasPressedButtonThisFrame(this.observeAction))
            {
                observableItem.Observe();
            }
        }

        private void UseObject(GameObject gameObject)
        {
            var interactableItem = gameObject.GetComponent<IFirstPersonInteract>();
            var interactObject = interactableItem?.InteractObject;

            if (interactableItem == null || interactObject == null)
            {
                this.SetDefaultValue();
                return;
            }

            if (this.showInteractionIcon)
            {
                this.pointerImage = interactObject.UseImage;
            }
            else
            {
                this.ShowTextPerLanguage(interactObject.UseText);
            }

            var useUiAction = uiMapActions.FindAction(interactObject.ActionName);
            var useAction = playerMapActions.FindAction(interactObject.ActionName);
            if (this.inputActionManager.WasPressedButtonThisFrame(useAction) || this.inputActionManager.WasPressedButtonThisFrame(useUiAction))
            {
                interactableItem.Interact();
            }
        }

        private void SetDefaultValue()
        {
            this.pointerImage = this.defaultPointerImage;
            LanguageService.StaticObserverTextMesh.text = "";
        }

        private void OnGUI()
        {
            if (showInteractionIcon)
            {
                if (this.pointerImage == this.defaultPointerImage)
                {
                    GUI.DrawTexture(new Rect((Screen.width / 2) - (Screen.width * 0.003515625f), (Screen.height / 2) - (Screen.height * 0.00625f), (Screen.width * 0.00703125f), (Screen.height * 0.0125f)), this.pointerImage);
                }
                if (this.pointerImage != this.defaultPointerImage)
                {
                    GUI.DrawTexture(new Rect((Screen.width / 2) - (Screen.width * 0.080859375f), (Screen.height / 2) - (Screen.height * 0.143055555f), (Screen.width * 0.00703125f) * 23.0f, (Screen.height * 0.0125f) * 23.0f), this.pointerImage);
                }
            }
            else
            {
                GUI.DrawTexture(new Rect((Screen.width / 2) - (Screen.width * 0.003515625f), (Screen.height / 2) - (Screen.height * 0.00625f), (Screen.width * 0.00703125f), (Screen.height * 0.0125f)), this.defaultPointerImage);
            }
        }

        private void ShowTextPerLanguage(string nameOfField)
        {
            this.TranslationService.ShowTextPerLanguage(nameOfField);
        }
    }
}