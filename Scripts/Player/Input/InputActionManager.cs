﻿using System;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine;

namespace Farmind.Player.Input
{
    [Serializable]
    public class InputActionManager
    {
        public static InputActionManager Instance => instance;
        protected static InputActionManager instance;

        public InputActionMap PlayerActionMap => this.playerActionMap;
        public InputActionMap UIActionMap => this.uiActionMap;
        protected InputActionMap playerActionMap;
        protected InputActionMap uiActionMap;

        public void SetSingleton()
        {
            if (instance == null)
            {
                instance = MonoBehaviour.FindObjectOfType<GameManagerController>().InputActionManager;
                if (instance == null)
                {
                    Debug.LogWarning("An instance of " + typeof(GameManagerController) + " is needed in the scene, but there is none.");
                }
            }
        }
        
        public void SetActionMaps()
        {
            var actions = GameObject.FindGameObjectWithTag("InputManager").GetComponent<PlayerInput>().actions;
            this.playerActionMap  = actions.FindActionMap("Player");
            this.uiActionMap = actions.FindActionMap("UI");
            this.playerActionMap.Enable();
            this.uiActionMap.Enable();
        }

        public bool WasPressedButtonThisFrame(InputAction inputAction)
        {
            if (inputAction != null)
            {
                var buttonControl = inputAction.activeControl as ButtonControl;
                if (buttonControl != null)
                {
                    if (buttonControl.wasPressedThisFrame)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool IsPressedButton(InputAction inputAction)
        {
            if (inputAction != null)
            {
                var buttonControl = inputAction.activeControl as ButtonControl;
                if (buttonControl != null)
                {
                    if (buttonControl.isPressed)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}