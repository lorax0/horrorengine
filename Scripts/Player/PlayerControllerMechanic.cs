﻿using UnityEngine;
using Farmind.Player.Mechanic;
using Farmind.Player.Input;
using System;
using UnityEngine.InputSystem;

namespace Farmind.Player
{
    public class PlayerControllerMechanic : MonoBehaviour
    {
        public IMechanicSystem MechanicSystem
        {
            get => this.mechanicSystem;
            set => this.mechanicSystem = (MechanicSystem)value;
        }

        [SerializeField] private MechanicSystem mechanicSystem;

        private event Action OnRightButtonClick;
        private event Action OnRightButtonHold;
        private event Action OnRightButtonRelease;
        private event Action OnLeftButtonClick;

        protected InputActionManager inputActionManager;
        protected InputAction pickupAction;
        protected InputAction throwAction;

        private void Start()
        {
            this.inputActionManager = InputActionManager.Instance;
            this.pickupAction = this.inputActionManager.PlayerActionMap.FindAction("Pickup");
            this.throwAction = this.inputActionManager.PlayerActionMap.FindAction("Throw");
            this.MechanicSystem.SetupMechanicCalculation();
            this.ActionSetup(this.MechanicSystem);
        }

        private void ActionSetup(IMechanicSystem mechanicSystem)
        {
            OnRightButtonClick = mechanicSystem.Pickup;
            OnRightButtonHold = mechanicSystem.Hold;
            OnRightButtonRelease = mechanicSystem.Drop;
            OnLeftButtonClick = mechanicSystem.Throw;
        }


        private void Update()
        {
            if (this.transform.hasChanged)
                this.MechanicSystem.PlayerTransform = this.transform;

            if (this.CanInteraction())
                this.ActionInvoke(this.MechanicSystem);
        }

        public bool CanInteraction()
        {
            var canMove = GameManager.Instance.HaveControl();
            return canMove;
        }


        public void ActionInvoke(IMechanicSystem mechanicSystem)
        {
            if (mechanicSystem.IsHoldingItem)
                ActionInvokeWhenHoldingItem();
            else
                ActionInvokeForEmptyHand();
        }
        
        private void ActionInvokeForEmptyHand()
        {
            bool isMouseRightButtonClick = inputActionManager.WasPressedButtonThisFrame(this.pickupAction);
            if (isMouseRightButtonClick)
                OnRightButtonClick.Invoke();
        }

        private void ActionInvokeWhenHoldingItem()
        {
            bool isMouseRightButtonHold = inputActionManager.IsPressedButton(this.pickupAction);
            bool isMouseRightButtonRelease = !inputActionManager.WasPressedButtonThisFrame(this.pickupAction);
            bool isMouseLeftButtonClick = inputActionManager.WasPressedButtonThisFrame(this.throwAction);

            if (isMouseRightButtonHold) OnRightButtonHold.Invoke();
            else if (isMouseRightButtonRelease) OnRightButtonRelease.Invoke();
            if (isMouseLeftButtonClick) OnLeftButtonClick.Invoke();
        }
    }
}
