﻿using Farmind.Objects;
using UnityEngine;

namespace Farmind.ShowText
{
    public class DisplayBookController : MonoBehaviour, IFirstPersonInteract
    {
        public InteractObject InteractObject => interactObject;

        [SerializeField] protected InteractObject interactObject;
        [SerializeField] protected DisplayBook displayBook;

        private void Start()
        {
            displayBook.InitializeBook();
        }

        public void Interact()
        {
            displayBook.ShowText();
        }
    }
}