﻿namespace Farmind.ShowText
{
    public interface IDisplayBook
    {
        void ShowText();
        void HideText();
        int GetNextPage();
        int GetPreviousPage();
        void ApplyPage(int pageNr);
        void InitializeBook();
    }
}