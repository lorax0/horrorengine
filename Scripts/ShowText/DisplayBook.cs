﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.ShowText
{
    [Serializable]
    public class DisplayBook : IDisplayBook
    {
        public int ActivePage => _activePage;

        [SerializeField] protected List<Texture2D> listOfPages = new List<Texture2D>();
        private int _activePage = 1;
        private GameManager gameManager;
        private BookManager bookManager;

        public void InitializeBook()
        {
            gameManager = GameManager.Instance;
            bookManager = BookManager.Instance;
        }
        
        public void ShowText()
        {
            bookManager.gameObject.SetActive(true);
            bookManager.SetBookReference(this);
            gameManager.LockControl();
            gameManager.UnlockScreen();
            ApplyPage(_activePage);
        }

        public void HideText()
        {
            gameManager.UnlockControl();
            gameManager.LockScreen();
            bookManager.gameObject.SetActive(false);
        }

        private bool IsNextPage()
        {
            return _activePage < listOfPages.Count;
        }
        
        private bool IsPreviousPage()
        {
            return _activePage > 1;
        }

        public int GetNextPage()
        {
            return IsNextPage() ? ++_activePage : _activePage;
        }

        public int GetPreviousPage()
        {
            return IsPreviousPage() ? --_activePage : _activePage;
        }

        private Sprite GetPage(int pageNr)
        {
            Texture2D pageTexture2D = listOfPages[pageNr - 1];
            Rect rectSection = new Rect(0,0, pageTexture2D.width, pageTexture2D.height);
            return Sprite.Create(pageTexture2D, rectSection, Vector2.down);
        }

        public void ApplyPage(int pageNr)
        {
            if (bookManager) bookManager.Image.sprite = GetPage(pageNr);
        }
    }
}