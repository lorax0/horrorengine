﻿using UnityEngine;
using UnityEngine.UI;

namespace Farmind.ShowText
{
    public class BookManager : MonoBehaviour
    {
        public Image Image => image;
        public static BookManager Instance => _instance;

        [SerializeField] protected Image image;
        private static BookManager _instance;
        private IDisplayBook displayBook;

        private void Awake()
        {
            if (_instance == null) _instance = this;
            else if (_instance != this) Destroy(gameObject);
            gameObject.SetActive(false);
        }

        public void SetBookReference(IDisplayBook book)
        {
            displayBook = book;
        }

        public void HideText()
        {
            displayBook.HideText();
        }

        public void NextPage()
        {
            displayBook.ApplyPage(displayBook.GetNextPage());
        }

        public void PreviousPage()
        {
            displayBook.ApplyPage(displayBook.GetPreviousPage());
        }
    }
}
