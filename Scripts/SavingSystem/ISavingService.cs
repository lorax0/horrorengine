﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Farmind.SavingSystem
{
    public interface ISavingService
    {
        string PlayerSavePath { get; set; }

        string DirectoryPath { get; set; }

        string GetSavingDirectory();

        bool DirectoryExist();

        void CreateDirectory();
    }
}