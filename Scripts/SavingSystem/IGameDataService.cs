using UnityEngine;

namespace Farmind.SavingSystem
{
    public interface IGameDataService
    {
        void SaveGame(ISavingService savingService);
        void LoadGame(ISavingService savingService);
    }
}