﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

namespace Farmind.SavingSystem
{
    [Serializable]
    public class SavingService : ISavingService
    {
        public string DirectoryPath { get => this.directoryPath; set => this.directoryPath = value; }
        public string PlayerSavePath { get => this.playerSavePath; set => this.playerSavePath = value; }
        [SerializeField] protected string directoryPath = "/saves/";
        [SerializeField] protected string playerSavePath = "player.txt";

        public string GetSavingDirectory()
        {
            var path = Application.persistentDataPath + this.directoryPath;
            return path;
        }

        public bool DirectoryExist()
        {
            var directory = this.GetSavingDirectory();
            var directoryExist = Directory.Exists(directory);
            return directoryExist;
        }

        public void CreateDirectory()
        {
            if (this.DirectoryExist())
            {
                return;
            }
            var directory = this.GetSavingDirectory();
            Directory.CreateDirectory(directory);
        }
    }
}
