﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using System.Collections.Generic;
using Farmind.Inventory;

namespace Farmind.SavingSystem
{
	public class GameDataController : MonoBehaviour
	{
		[SerializeField] protected GameDataService savingManager;
        [SerializeField] protected SavingService savingService;
        private InputActionMap actionMap;
        private InputAction saveInventoryAction;
        private InputAction loadInventoryAction;

		private void Awake()
        {
			var inputActions = GameObject.FindGameObjectWithTag("InputManager").GetComponent<PlayerInput>().actions;
            this.actionMap = inputActions.FindActionMap("Player");
            this.saveInventoryAction = this.actionMap.FindAction("Save Inventory");
            this.loadInventoryAction = this.actionMap.FindAction("Load Inventory");
            this.savingManager.SetListOfSaveableObjects();
        }

        private void Update()
		{
			if (actionMap != null)
			{
                if (this.IsButtonPress(this.saveInventoryAction))
                {
					this.savingManager.SaveGame(this.savingService);
                }
				if (this.IsButtonPress(this.loadInventoryAction))
				{
					this.savingManager.LoadGame(this.savingService);
				}

			}
		
		}

        private bool IsButtonPress(InputAction action)
		{
			var button = action.activeControl as ButtonControl;
			if (action != null)
			{
				if (button != null)
				{
					if (button.wasPressedThisFrame)
					{
						return true;
					}
				}
			}
			return false;
		}
        
	}
}