﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Farmind.Inventory;
using System;
using Farmind.Player;

namespace Farmind.SavingSystem
{
    [Serializable]
    public class GameDataService : IGameDataService
    {
        [SerializeField] protected PlayerController player;
        protected IPlayerData playerData;

        public void SetListOfSaveableObjects()
        {
            if(this.player != null)
            {
                this.playerData = this.player.PlayerData;
            }
        }

        public void SaveGame(ISavingService savingService)
        {
            var directory = savingService.GetSavingDirectory();
            savingService.CreateDirectory();
            playerData.Save(directory + savingService.PlayerSavePath);
        }
		
		public void LoadGame(ISavingService savingService)
        {
            var directory = savingService.GetSavingDirectory();
            playerData.Load(directory + savingService.PlayerSavePath);
        }
    }
}
