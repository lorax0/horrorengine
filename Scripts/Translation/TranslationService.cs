﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Farmind.Translation
{
    [Serializable]
    public class TranslationService
    {
        public List<TranslationLanguage> Translations { get { return this.translations; } set { this.translations = value; } }
        
        [SerializeField]
        private List<TranslationLanguage> translations;

        public string GetTranslatedText(string nameOfField)
        {
            string language = LanguageService.GameLanguage.ToString();
            if(translations != null)
            {
                TranslationLanguage translationLanguage = this.translations.FirstOrDefault(x => x.Language == language);
                var translationField = translationLanguage?.GetTranslationFieldByName(nameOfField);
                if (translationField != null)
                {
                    return translationField.Translation;
                }
            }
         

            return null;
        }

        public void ShowTextPerLanguage(string nameOfField)
        {
            var translation = this.GetTranslatedText(nameOfField);
            if (translation != null)
            {
                LanguageService.StaticObserverTextMesh.text = translation;
            }
            else
            {
                LanguageService.StaticObserverTextMesh.text = "Translation not found for " + nameOfField;
            }
        }
    }
}