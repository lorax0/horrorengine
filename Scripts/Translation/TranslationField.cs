﻿using System;
using UnityEngine;

namespace Farmind.Translation {
    [Serializable]
    public class TranslationField {

        [SerializeField]
        private String name;

        [SerializeField, TextArea]
        private String translation;

        public String Name {
            get { return this.name; }
            set { this.name = value; }
        }

        public String Translation {
            get { return this.translation; }
            set { this.translation = value; }
        }

        public String Label {
            get { return name + ": " + translation; }
        }

    }

}