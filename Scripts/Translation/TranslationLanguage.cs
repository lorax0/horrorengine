﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Farmind.Translation {
    
    [CreateAssetMenu (fileName = "Translation", menuName = "Farmind/Translation", order = 0)]
    public class TranslationLanguage : ScriptableObject {
        public String Language { get { return this.language;} set { this.language = value; } }

        public List<TranslationField> TranslationFields {
            get {
                return translationFields;
            }
            set
            {
                this.translationFields = value;
            }
        }

        [SerializeField]
        protected string language;

        [SerializeField]
        protected List<TranslationField> translationFields = new List<TranslationField>();

        public bool Equals (string language) => String.Equals (language, this.language);

        public override bool Equals (object obj) {
            return base.Equals (obj);
        }

        public override int GetHashCode () {
            return base.GetHashCode ();
        }

        public override string ToString () {
            return this.language;
        }

        public TranslationField GetTranslationFieldByName(string name) => translationFields.FirstOrDefault( x => x.Name == name);           
        
    }

}