﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

namespace Farmind.Translation
{
    [Serializable]
    public class LanguageService
    {
        public static SystemLanguage GameLanguage = SystemLanguage.English;
        public static Text StaticObserverTextMesh;
        public static Text StaticCustesceneTextMesh;
        public static GameObject ShowTextObject;
        public static GameObject ShowTextSprite;
        public static int ShowTextID;

        public SystemLanguage DebugLanguage = SystemLanguage.English;
        public bool IsDebugging;
        public Text ObserveTextMesh;
        public Text CutsceneTextMesh;

        [SerializeField]
        private List<SystemLanguage> languages;

        public bool IsLanguageAvaible(SystemLanguage language)
        {
            foreach (SystemLanguage l in languages)
            {
                if (l.Equals(language))
                {
                    return true;
                }
            }

            return false;
        }

        public void InitlizeLanguage()
        {
            StaticObserverTextMesh = ObserveTextMesh;
            StaticCustesceneTextMesh = CutsceneTextMesh;
            ShowTextObject = null;
            ShowTextSprite = null;
            ShowTextID = 0;

            if (IsDebugging)
            {
                GameLanguage = DebugLanguage;
            }
            else
            {
                GameLanguage = IsLanguageAvaible(Application.systemLanguage) ? Application.systemLanguage : DebugLanguage;
            }
        }
    }
}