﻿using UnityEngine;
using Farmind.Translation;

public class LanguageManager : MonoBehaviour
{
    [SerializeField]
    private LanguageService languageService;

    void Awake()
    {
        this.languageService.InitlizeLanguage();
    }
}
