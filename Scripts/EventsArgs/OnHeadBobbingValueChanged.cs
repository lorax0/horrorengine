﻿using System;
using UnityEngine;

namespace Farmind.EventsArgs
{
    public class OnHeadBobbingValueChanged : EventArgs
    {
        public Vector3 OffSet;
    }
}
