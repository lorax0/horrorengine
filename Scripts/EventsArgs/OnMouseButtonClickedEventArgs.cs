using System;
using UnityEngine;

namespace Farmind.EventsArgs
{
    public class OnMouseButtonClickedEventArgs : EventArgs
    {
        public Vector2 Position;
    }
}